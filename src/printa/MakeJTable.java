/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package printa;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.StringTokenizer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea; 
import javax.swing.JTextField;

/**
 *
 * @author Kimenyi
 */
public class MakeJTable extends JFrame{
int LENTH=600;
int WIDT =600;
JButton viewButtton,openButton,sendButton,updateButton, visualizeButton,varsUpadateButton;
JTextArea grab;
JTextArea resulat; 
JLabel ID_FCTL,R_FCTL,G_FCTL,B_FCTL,DIMINUTION_FCTL,
NAME_FCTL,FROM_FCTL,WHERE_FCTL,ORDER_FCTL,COLOR_FCTL;

JTextField ID_FCT,R_FCT,G_FCT,B_FCT,DIMINUTION_FCT,
NAME_FCT,FROM_FCT,WHERE_FCT,ORDER_FCT,COLOR_FCT;
JList showVariable= new JList();
ResultSet outResult;
Connection conn = null;
Statement s; 
PreparedStatement psInsert;
String connectionURL ;
String server;
String dbName;
 ShowVar [] controlVar;
 LinkedList <ShowVar> varList=new LinkedList();;
MakeJTable(String server,String dbName)
{
    this.dbName=dbName;
    this.server=server;
connectionURL = "jdbc:derby://"+server+":1527/" + dbName + ";create=false";
  try { 
    conn = DriverManager.getConnection(connectionURL);		 
    s = conn.createStatement(); 
    }
    catch (Throwable e){
        ;
    }
draw();
grab();
open();
send();
visualize();
varsUpadate();
update();
} 

public void draw()
{
setTitle("Ishyiga JBuilder"); 
this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
Container content = getContentPane(); 
 JPanel panel = new JPanel();
 panel.setLayout(new GridLayout(3,1)); 
panel.add(northPane());
panel.add(centerPane());
panel.add(southPane());
content.add(panel);
 
setSize(WIDT,LENTH); 
setVisible(true);  
}
public void grab()
{
viewButtton.addActionListener(
new ActionListener()
{  public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==viewButtton)
{    
String[] lines = grab.getText().split("\n");

String res="res : "+lines.length;

for(int i=0;i<lines.length;i++)
{
     String datatype="";
    String table="" ;
       int length=0;
    System.out.println(lines[i]);
    if(lines[i].contains("create table"))
      {
          table=lines[i];       
          table= table.replaceAll("create table", "");
             res=res+"  THE TABLE IS "+table+"\n";
             if(table.contains("."))
             {
              StringTokenizer st1=new StringTokenizer (table);
              st1.nextToken("."); 
              table=st1.nextToken(""); 
              table=table.replace('.', ' ');
              NAME_FCT.setText(table);                      
              FROM_FCT.setText("FROM "+table);
             }
             
             
      }
      System.out.println(lines[i]);
    StringTokenizer st1=new StringTokenizer (lines[i]);
    if(lines[i].length()>1)
    {   String var = st1.nextToken(" "); 
   
  System.out.println(var);
  System.out.println(var.replaceAll("	", ""));
    if(lines[i].contains("VARCHAR"))
    {
        datatype="VARCHAR" ;
        String resLength="";
        boolean done=false;
        for(int j=0;j<lines[i].length();j++)
        { char c=lines[i].charAt(j);
        if(c==')')
               break;
        if(done)
            resLength=resLength+c;
            if(c=='(')
                done=true; 
        } 
           res=res+var+"  - "+datatype+"  - "+resLength+"\n"; 
 varList.add(new ShowVar(0,var,datatype,Integer.parseInt(resLength),datatype,var)); 
 
    }
    else if(lines[i].contains("INTEGER"))
    {
        datatype="INTEGER";
           res=res+var+"  - "+datatype+"\n";
varList.add(new ShowVar(0,var,datatype,0,datatype,var)); 
 
    }
    else  if(lines[i].contains("DOUBLE"))
    {      datatype="DOUBLE ";   
    
    res=res+var+"  - "+datatype+"\n";
    varList.add(new ShowVar(0,var,datatype,0,datatype,var)); 
 
    }
    }
} 
    resulat.setText(res); 
    controlVar=new ShowVar[varList.size()];
            for(int i=0;i<varList.size();i++)
{
    controlVar [i]=varList.get(i);
}
showVar();
}
}});
}  

void showVar()
{ 
    showVariable.setListData(controlVar);
}
boolean isInteger(char c)
{
if(c==0)
    return true;
else if(c==1)
    return true;
else if(c==2)
    return true;
else if(c==3)
    return true;
else if(c==4)
    return true;
else if(c==5)
    return true;
else if(c==6)
    return true;
else if(c==7)
    return true;
else if(c==8)
    return true;
else if(c==9)
    return true;
else
    return false;




}    


JPanel northPane(   )
{ 
    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(4,5)); 
     
     ID_FCTL= new JLabel ("ID");
     R_FCTL= new JLabel ("R");
     G_FCTL= new JLabel ("G");
     B_FCTL= new JLabel ("B");
     DIMINUTION_FCTL= new JLabel ("DIMIN");
NAME_FCTL= new JLabel ("NAME");
FROM_FCTL= new JLabel ("FROM");
WHERE_FCTL= new JLabel ("WHERE");
ORDER_FCTL= new JLabel ("ORDER");
COLOR_FCTL= new JLabel ("COLOR");

ID_FCT=new JTextField();R_FCT=new JTextField("255");
 G_FCT=new JTextField("255");B_FCT=new JTextField("255");
 DIMINUTION_FCT=new JTextField("15");NAME_FCT=new JTextField();
 FROM_FCT=new JTextField();WHERE_FCT=new JTextField();
 ORDER_FCT=new JTextField();COLOR_FCT=new JTextField("B_FCT");
 
panel.add( ID_FCTL);panel.add( R_FCTL);panel.add( G_FCTL);panel.add( B_FCTL);panel.add( DIMINUTION_FCTL);
panel.add( ID_FCT);panel.add( R_FCT);panel.add( G_FCT);panel.add( B_FCT);panel.add( DIMINUTION_FCT);

panel.add( NAME_FCTL);panel.add( FROM_FCTL);panel.add( WHERE_FCTL);panel.add( ORDER_FCTL);panel.add( COLOR_FCTL);
panel.add( NAME_FCT);panel.add( FROM_FCT);panel.add( WHERE_FCT);panel.add( ORDER_FCT);panel.add( COLOR_FCT);

  return panel;
}    
JPanel centerPane(  )
{
 
    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(2,1)); 
    
    grab=new JTextArea(20,20);
    grab.setBackground(Color.lightGray);
    
    resulat=new JTextArea(20,20);
    varsUpadateButton=new JButton("VARS");
    JPanel panel2 = new JPanel(); 
    
   JScrollPane jg =new JScrollPane(grab);
   jg.setPreferredSize(new Dimension(100,100));
   JScrollPane jr =new JScrollPane(showVariable);
   jr.setPreferredSize(new Dimension(300,100));
   panel.add( jg);
   
    panel2.add( jr);
     panel2.add( varsUpadateButton);
   panel.add( panel2);
   
    return panel;
}   
    
JPanel southPane(  )
{
 
    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(1,5)); 
       viewButtton=new JButton("VIEW");
    openButton=new JButton("OPEN");
    visualizeButton=new JButton("VISUALIZE");
    sendButton=new JButton("SEND");
    updateButton=new JButton("UPDATE"); 
     
   panel.add( viewButtton);      
   panel.add( openButton);
   panel.add( sendButton);      
   panel.add( updateButton);
   panel.add( visualizeButton);
   
    return panel;
}


public void visualize()
{

visualizeButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==visualizeButton)
{
   new ShowDatabase(JOptionPane.showInputDialog(null, "Fungura umubare ")," ",dbName,server);  ; 
  
  
}
} 
});
}

public void open()
{

openButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==openButton)
{
  getVars((JOptionPane.showInputDialog(null, "Fungura umubare "))) ;  
}
} 
});
}

public void varsUpadate()
{ 
varsUpadateButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==varsUpadateButton && showVariable.getSelectedValue()!=null)
{
   
      ShowVar v = (ShowVar)showVariable.getSelectedValue();
      int index = showVariable.getSelectedIndex();
      
v.VALEUR_VAR= JOptionPane.showInputDialog (null,"CHANGE VALEUR",v.VALEUR_VAR);
v.CAT_VAR   =  JOptionPane.showInputDialog(null,"CHANGE CAT"   ,v.CAT_VAR);
v.LENGTH_VAR=Integer.parseInt(JOptionPane.showInputDialog(null,"LENGTH ",(int)v.LENGTH_VAR));

controlVar [index]=v;
showVar(); 
}
}});
}
public void update()
{

updateButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==updateButton)
{
try
{ 
int fact=Integer.parseInt(ID_FCT.getText()); 
s = conn.createStatement();
String createString = " update APP.SHOW_FCT set NAME_FCT='" +NAME_FCT.getText()+ "',"
        + "FROM_FCT='" +FROM_FCT.getText()+ "',"
        + "ORDER_FCT='" +ORDER_FCT.getText()+ "',COLOR_FCT='" +COLOR_FCT.getText()+ "', "
        + "DIMINUTION_FCT=" +DIMINUTION_FCT.getText()+ " ,"
        + "R_FCT=" +R_FCT.getText()+ ",G_FCT=" +G_FCT.getText()+ ",B_FCT=" +B_FCT.getText()+ " where ID_FCT=" + fact;
 s.execute(createString);

s = conn.createStatement();
String createString1 = " delete from APP.SHOW_VAR   where ID_FCT=" + fact ; 
s.execute(createString1); 
insertVars(fact); 
}
catch (Throwable e)  {   
System.err.println(" "+e) ;
}  

}
} });
}
public void send()
{
sendButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==sendButton)
{ 
try
{ 
 
psInsert = conn.prepareStatement("insert into APP.SHOW_FCT "
    + "(NAME_FCT,FROM_FCT ,WHERE_FCT,ORDER_FCT,R_FCT,G_FCT,B_FCT,COLOR_FCT,DIMINUTION_FCT )  values (?,?,?,?,?,?,?,?,?)");
 
psInsert.setString(1,NAME_FCT.getText());
psInsert.setString(2,FROM_FCT.getText());
psInsert.setString(3,WHERE_FCT.getText());
psInsert.setString(4,ORDER_FCT.getText());
psInsert.setInt(5,Integer.parseInt(R_FCT.getText()));
psInsert.setInt(6,Integer.parseInt(G_FCT.getText()));
psInsert.setInt(7,Integer.parseInt(B_FCT.getText()));
psInsert.setString(8,COLOR_FCT.getText()); 
psInsert.setInt(9,Integer.parseInt(DIMINUTION_FCT.getText())); 
psInsert.executeUpdate();
psInsert.close();
 
psInsert = conn.prepareStatement("insert into APP.SHOW_VAR "
+ "(ID_FCT,VALEUR_VAR ,CAT_VAR,LENGTH_VAR,VISIBLE_VAR,TITLE_VAR )  values (?,?,?,?,?,?)");
int fact=0; 
outResult =  s.executeQuery("select max(ID_FCT) from SHOW_FCT  "); 
while ( outResult.next())
{ 
 fact = outResult.getInt(1);
} 
  outResult.close();
insertVars(fact);
}
catch (Throwable e)  {   
System.err.println(" "+e) ;
}  
}
}});
}
void insertVars(int fact)
{
  try
    {   
psInsert = conn.prepareStatement("insert into APP.SHOW_VAR "
+ "(ID_FCT,VALEUR_VAR ,CAT_VAR,LENGTH_VAR,VISIBLE_VAR,TITLE_VAR )  values (?,?,?,?,?,?)");
 

for (int i=0;i<controlVar.length;i++)
{
ShowVar v=controlVar[i];
psInsert.setInt(1,fact);
psInsert.setString(2,v.VALEUR_VAR);
psInsert.setString(3,v.CAT_VAR);
psInsert.setInt(4,(int)v.LENGTH_VAR);
psInsert.setString(5,v.VISIBLE_VAR);
psInsert.setString(6,v.TITLE_VAR); 

psInsert.executeUpdate();  
}
}
catch (Throwable e)  {   
System.err.println(" "+e) ;
}  
}

void getVars(String fct)
{ 
    varList=new LinkedList();
    try
    { 
    outResult = s.executeQuery("select * from SHOW_FCT where ID_FCT =  "+fct+"  "); 
    while (outResult.next())
    {   
ID_FCT.setText(""+outResult.getInt("ID_FCT"));
R_FCT.setText(""+outResult.getInt("R_FCT"));
 G_FCT.setText(""+outResult.getInt("G_FCT"));
 B_FCT.setText(""+outResult.getInt("B_FCT")); 
 DIMINUTION_FCT.setText(""+outResult.getInt("DIMINUTION_FCT"));
 NAME_FCT.setText(outResult.getString("NAME_FCT"));
 FROM_FCT.setText(outResult.getString("FROM_FCT"));
 WHERE_FCT.setText(outResult.getString("WHERE_FCT"));
 ORDER_FCT.setText(outResult.getString("ORDER_FCT"));
 COLOR_FCT.setText(outResult.getString("COLOR_FCT"));
    } 
    outResult.close();
        
    outResult = s.executeQuery("select * from SHOW_VAR where ID_FCT =  "+fct+"  "); 
    while (outResult.next())
    {   
varList.add(new ShowVar(Integer.parseInt(fct),outResult.getString("VALEUR_VAR"),outResult.getString("CAT_VAR"),outResult.getDouble("LENGTH_VAR"),outResult.getString("VISIBLE_VAR"),outResult.getString("TITLE_VAR")));
 } 
     controlVar=new ShowVar[varList.size()];
for(int i=0;i<varList.size();i++)
{
    controlVar [i]=varList.get(i);
}
      showVar();
    outResult.close();    
    }  catch (Throwable e)  {
        System.err.println(""+e);
    }
    
//return id_fct;// getDataButton(  query, varList) ; 
}

public static void main(String[] arghs)  
{
    new MakeJTable("localhost","kipharmaOk");
    
}

}
