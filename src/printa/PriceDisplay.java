 
package printa;
 
import java.io.*;
import java.util.*;
import javax.comm.*;

/**
 * Class declaration
 *
 *
 * @author
 * @version 1.10, 08/04/00
 */
public class PriceDisplay {
    static Enumeration	      portList;
    static CommPortIdentifier portId;
    static String	      messageString = " sdsds";
    static SerialPort	      serialPort;
    static OutputStream       outputStream;
    static boolean	      outputBufferEmptyFlag = false;
    /**
     * Method declaration
     *
     *
     * @param args
     *
     * @see
     */
    public static void main(String[] args) {
boolean portFound = false;
String  defaultPort = "COM3";
System.out.println(defaultPort);
if (args.length > 0) {
defaultPort = args[0];
} 
//System.out.println(CommPortIdentifie);
	portList = CommPortIdentifier.getPortIdentifiers();
//System.out.println(portList.hasMoreElements());
	while (portList.hasMoreElements()) {
            System.out.println();
	    portId = (CommPortIdentifier) portList.nextElement();
System.out.println("id "+portId.getName());
	    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) 
            {

		if (portId.getName().equals(defaultPort)) {
		    System.out.println("Found port " + defaultPort);

		    portFound = true;

		    try {
			serialPort = 
			    (SerialPort) portId.open("PriceDisplay", 2000);
                        
		    } catch (PortInUseException e) {
			System.out.println("Port in use.");

			continue;
		    } 

		    try {
			outputStream = serialPort.getOutputStream();
		    } catch (IOException e) {}

		    try {
                            serialPort.setSerialPortParams(9600, 
                            SerialPort.DATABITS_8, 
                            SerialPort.STOPBITS_1, 
                            SerialPort.PARITY_NONE);
                        } catch (UnsupportedCommOperationException e) {} 
		    try {
		    	serialPort.notifyOnOutputEmpty(true);
		    } catch (Exception e) {
			System.out.println("Error setting event notification");
			System.out.println(e.toString());
			System.exit(-1);
		    } 
		    System.out.println(
		    	"Writing \""+messageString+"\" to "
			+serialPort.getName());

		    try {
			outputStream.write(28);
		    } catch (IOException e) {}

		    try {
		       Thread.sleep(100);  // Be sure data is xferred before closing
		    } catch (Exception e) {}
		    serialPort.close();
		    System.exit(1);
		} 
	    } 
	} 

	if (!portFound) {
	    System.out.println("port " + defaultPort + " not found.");
	} 
    } 


}




