    package printa;



    import java.util.LinkedList;

    /* Invoice for Ishyiga copyright 2007 Kimenyi Aimable Gestion des factures
    */


    class Invoice
    {
    String id_employe,date,id_client,heure,id,mode,status,reference;
    private LinkedList <Product>productList,productListDB;
    private int total;
    int payed,tot,id_invoice;
    int tva;
    String []footer;
    Client client;
    
    public Invoice(String id_employe,String id_client,String date)
    {


    this.id_employe=id_employe;
    this.productList= new LinkedList();
    this.date=date;
    total=0;
    this.id_client=id_client;
    }
    public Invoice(String id_employe,String date,int total,String id_client)
    {
    this.date=date;
    this.id_employe=id_employe;
    this.tot=total;
    this.id_client=id_client;
    }

    public Invoice(int id,String id_employe,String date, String id_client,int total,String heure,int tva)
    {
    this.id_invoice=id;
    this.heure=heure;
    this.date=date;
    this.id_employe=id_employe;
    this.tot=total;
    this.id_client=id_client;
    this.tva=tva;
    }

      public Invoice(int id,String id_employe,String date, String id_client,int total,String heure,int tva,String mode,String status)
    {
    this.id_invoice=id;
    this.heure=heure;
    this.date=date;
    this.id_employe=id_employe;
    this.tot=total;
    this.id_client=id_client;
    this.tva=tva;
    this.status=status;
    this.mode=mode;
    }
    public void fixIdInvoice(int i)
    {this.id_invoice=i;}

    public LinkedList <Product> getProductList()
    {
    return productList;
    }

    public void add(Product productToAdd)
    {
    productList.add(productToAdd);
    total=+(int)productToAdd.currentPrice;
    }
    public LinkedList <Product> getProductListDB()
    {
    productListDB= new LinkedList();


    for(int i=0;i<productList.size();i++)
    {
    Product pi= (Product)(productList.get(i))  ;
    
    boolean done=true;

    for(int j=0;j<productListDB.size();j++)
    {
    Product p= (Product)(productListDB.get(j))  ;
    if(pi.productCode()== p.productCode())
    {

    p.qty=p.qty+pi.qty;


    done=false;

    }
    }
    if(done==true)
    {
        Product gu=pi.clone(pi.qty);
        
        productListDB.add(gu);
    }
    
    
    }



    return productListDB;
    }
    public void remove(Product p)
    {
    boolean done=true;
    for(int i=0;i<productList.size();i++)
    {
    Product pi= productList.get(i)  ;
    if(pi.productCode()== p.productCode() && done== true && pi.qty ()== p.qty())
    {
    productList.remove(i);
    done=false;
    total=total- ((int)p.currentPrice*p.qty());
    }
    }
    }
    @Override
    public String toString()
    {
    return id_invoice+ "  "+ id_client+"   "+tot ;
    }
    public int total()
    {
    int t = 0;
    for(int i=0;i<productList.size();i++)
    {
    Product pi= productList.get(i)  ;
    t=t+ ((int)pi.currentPrice*pi.qty());

    }

    return t;
    }
    }