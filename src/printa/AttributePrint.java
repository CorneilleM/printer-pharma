/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package printa;

import java.text.AttributedCharacterIterator.Attribute;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

/**
 *
 * @author Kimenyi
 */
public class AttributePrint implements PrintRequestAttributeSet
{
    
    public boolean add(Attribute attribute)
  {
    
    
    return true;
    
    }
    /**
     * Adds all of the elements in the specified set to this attribute.
     * The outcome is  the same as if the
     * {@link #add(Attribute) <CODE>add(Attribute)</CODE>} 
     * operation had been applied to this attribute set successively with
     * each element from the specified set. If none of the categories in the
     * specified set  are the same as any categories in this attribute set, 
     * the <tt>addAll()</tt> operation effectively modifies this attribute
     * set so that its value is the <i>union</i> of the two sets. 
     * <P>
     * The behavior of the <CODE>addAll()</CODE> operation is unspecified if  
     * the specified set is modified while the operation is in progress.
     * <P>
     * If the <CODE>addAll()</CODE> operation throws an exception, the effect
     * on this attribute set's state is implementation dependent; elements  
     * from the specified set before the point of the exception may or 
     * may not have been added to this attribute set. 
     *
     * @param  attributes  whose elements are to be added to this attribute 
     *            set.
     *
     * @return  <tt>true</tt> if this attribute set changed as a result of
     *          the call.
     * 
     * @throws  UnmodifiableSetException
     *     (Unchecked exception) Thrown if this attribute set does not
     *     support the <tt>addAll()</tt> method. 
     * @throws  ClassCastException
     *     (Unchecked exception) Thrown if some element in the specified 
     *     set is not an instance of interface {@link PrintRequestAttribute 
     *     PrintRequestAttribute}.
     * @throws  NullPointerException
     *     (Unchecked exception) Thrown if the specified  set is null. 
     *
     * @see #add(Attribute)
     */
    public boolean addAll(AttributeSet attributes)
            
    {
    
    return true;
    }

    public boolean add(javax.print.attribute.Attribute attribute) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public javax.print.attribute.Attribute get(Class<?> category) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean remove(Class<?> category) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean remove(javax.print.attribute.Attribute attribute) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean containsKey(Class<?> category) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean containsValue(javax.print.attribute.Attribute attribute) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int size() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public javax.print.attribute.Attribute[] toArray() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void clear() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
