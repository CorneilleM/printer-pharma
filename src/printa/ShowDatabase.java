
 
package printa;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.StringTokenizer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane; 
import javax.swing.JTable;

/**
 *
 * @author Kimenyi
 */
public class ShowDatabase extends JFrame {

String driver = "org.apache.derby.jdbc.NetWorkDriver"; 
String dbName;  String  date,mois; 
String connectionURL= "";
ResultSet outResult;
Connection conn = null;
Statement s;
public String currentQuery="";
public String currentQueryMid="";
public String currentQueryOrder="";
String value="";
  LinkedList <ShowVar> varList;
  LinkedList <JList> linJlist= new LinkedList ();
  JPanel mainPaneUp=new JPanel();
  JPanel mainPaneDown=new JPanel();
  Container content=null;
  int R_FCT=0;
  int G_FCT=0;
  int B_FCT=0;
  int DIMINUTION_FCT=0;
  String COLOR_FCT="";
  final int  W=1000;
  final int  H=750;
  int totalLength=0;
JMenuBar menu_bar1 = new JMenuBar(); 
JMenu menu1 = new JMenu("Fichier");
JMenuItem table = new JMenuItem("Voir sous forme table ");
JMenuItem print = new JMenuItem("Imprimer le document selectionné");
JMenuItem exporter = new JMenuItem("Exporter la facture selectionnée ");
JMenuItem email = new JMenuItem("Envoyer la facture selectionnée par Email ");
JTable jTable;

public ShowDatabase(String fct,String mid,String dbName,String server)
{ 
   connectionURL = "jdbc:derby://"+server+":1527/" + dbName + ";create=true";  
read(); 
currentQueryMid=mid;
draw(fct);
menu1.add(table);
menu1.add(exporter);
menu1.add(print); 
menu_bar1.add(menu1);
this.setJMenuBar(menu_bar1);
table();
}
 public void draw(String fct)
    {
  this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); 

//on fixe  les composant del'interface
  content = getContentPane(); 
getVars(fct); 
mainPaneUp.setPreferredSize(new Dimension(W,40));
getDataButton();
content.add(Pane()); 
setSize(W,H);
setVisible(true);
    }
public final  void read()
{ 
try { 
conn = DriverManager.getConnection(connectionURL);
s = conn.createStatement(); 
}
catch (Throwable e){ 
System.out.println("Throwable :"+e);}
} 

void table(){
table.addActionListener(new ActionListener()
{
public void actionPerformed(ActionEvent e)
{
 if(e.getSource()==table   )
{
    
    
  JOptionPane.showConfirmDialog(null, new showJTable(jTable),"TABLE", JOptionPane.NO_OPTION);
 
}
}
});
}
//toutes les selection zizajya zibera hano    
// tableau des données     
    

void getVars(String fct)
{
    String query="";
    int id_fct=0;
   varList= new LinkedList();
    try
    { 
    outResult = s.executeQuery("select * from SHOW_FCT where ID_FCT = "+fct+" "); 
     System.err.println("select * from SHOW_FCT where NAME_FCT = '"+fct+"' ");
    while (outResult.next())
    {   
     id_fct=outResult.getInt("ID_FCT");   
     currentQuery= outResult.getString("FROM_FCT")+" "+outResult.getString("WHERE_FCT")+" ";
     currentQueryOrder=outResult.getString("ORDER_FCT") ;
     R_FCT=outResult.getInt("R_FCT");
     G_FCT=outResult.getInt("G_FCT");
     B_FCT=outResult.getInt("B_FCT");
     DIMINUTION_FCT=outResult.getInt("DIMINUTION_FCT");;
     COLOR_FCT=outResult.getString("COLOR_FCT");
    }  
    if(id_fct!=0)
    {
    outResult = s.executeQuery("select * from SHOW_VAR where ID_FCT = "+id_fct+" "); 
     System.out.println("select * from SHOW_VAR where ID_FCT = "+id_fct+" ");
    while (outResult.next())
    {   
        // System.out.println(outResult.getString("TITLE_VAR"));
   varList.add(new ShowVar(id_fct,outResult.getString("VALEUR_VAR"),outResult.getString("CAT_VAR"),outResult.getDouble("LENGTH_VAR"),outResult.getString("VISIBLE_VAR"),outResult.getString("TITLE_VAR")));
        totalLength=totalLength+(int)outResult.getDouble("LENGTH_VAR") ;
   } 
    totalLength=totalLength+50;
    }
    outResult.close();
    }  catch (Throwable e)  {
        System.err.println(""+e);
    }
    
//return id_fct;// getDataButton(  query, varList) ; 
}


public void getDataButton()
{ 
String vars=""; 

for (int i=0;i<varList.size();i++)  
{
    System.out.println(i);  
    ShowVar sv=varList.get(i);
    String virgule=",";
    if(i==0)
        virgule=""; 
     value=sv.VALEUR_VAR;
 vars=vars+ virgule+value;  
Font police = new Font("Rockwell", Font.PLAIN, 9);
 JButton button=new JButton(sv.TITLE_VAR);
 button.setFont(police);
 button.setPreferredSize(new Dimension( (int)((W-100)*sv.LENGTH_VAR)/totalLength ,30));
  ActionListener action = new ActionListener()
    {  public void actionPerformed(ActionEvent ae)
    {   
    currentQueryOrder=" order by "+orderBy((((JButton)ae.getSource()).getText())); 
       inPutListSecond(getDataList(currentQuery+currentQueryMid+currentQueryOrder));
    }};
 button.addActionListener(action); 
 mainPaneUp.add(button);  
}     
currentQuery="SELECT "+vars+" "+currentQuery;
System.out.println(currentQuery);  
  
}
String orderBy(String txt)
{
  for (int i=0;i<varList.size();i++)  
{
    ShowVar sv=varList.get(i);
if(sv.TITLE_VAR.equals(txt))
    
    return sv.VALEUR_VAR;
}    
    return "";
} 
public JPanel Pane()
{
    
JPanel mainPane=new JPanel();

mainPane.add(mainPaneUp,BorderLayout.NORTH); 
inPutListFirst(getDataList(currentQuery+currentQueryMid+currentQueryOrder));
JScrollPane scrollmainPaneDown =new JScrollPane(mainPaneDown);
scrollmainPaneDown.setPreferredSize(new Dimension(W-10,H-50));
mainPane.add(scrollmainPaneDown,BorderLayout.WEST);
 
return mainPane;
}
public String [][] getDataList(String query)
{ 
    mainPaneDown=new JPanel();
String [][]  tab = new String  [varList.size()][500000];
String [][] tab2 = new String  [500000][varList.size()];
 
 String []entete= new String  [varList.size()];
 int []len= new int  [varList.size()];
 int totalLen=0;
int j=0;
try
{   

outResult = s.executeQuery(query);  
 
while (outResult.next())
{   
j++;
for (int i=0;i<varList.size();i++)  
{
String valeur= varList.get(i).VALEUR_VAR;
String cat=varList.get(i).CAT_VAR;
String out="";
int uburebure=(int)varList.get(i).LENGTH_VAR;
entete[i]=valeur; 
len[i]=uburebure;
totalLen=totalLen+uburebure;
//
if(cat.contains("V"))
{
  out  = outResult.getString(valeur) ;
} 
else if (cat.contains("I") && ! valeur.contains("."))
{
//System.out.println("+++++++++++++++++++++++++++++++++++++++++"+ou***t);if
    if(varList.get(i).VISIBLE_VAR.contains("E")) 
out  =  setVirgule(outResult.getInt(valeur)) ;  
    else
 
    out  =""+ outResult.getInt(valeur) ;
        
 
} 
else if (cat.contains("D"))
{
 out =""+ outResult.getDouble(valeur) ;
}
tab[i][j]=out; 
tab2[j][i]=out; 
}   
}

    outResult.close();
    }  catch (Throwable e)  {
        System.err.println(""+e);
    }
String [][] tab1 = new String  [varList.size()][j]; 
for(int i=0;i<varList.size();i++)
        System.arraycopy(tab[i], 0, tab1[i], 0, j);
String [][] tab3 = new String [j] [varList.size()]; 

for(int i=0;i<j;i++)
        System.arraycopy(tab2[i], 0, tab3[i], 0, varList.size());


        
  jTable = new javax.swing.JTable();
    jTable.setModel(new javax.swing.table.DefaultTableModel(tab3,entete  ) );
    
    
   jTable.setAutoResizeMode(jTable.AUTO_RESIZE_ALL_COLUMNS);
   
   if(totalLen==0)
       totalLen=W;
   for(int i=0;i<len.length;i++)
            jTable.getColumnModel().getColumn(i).setPreferredWidth((len[i]*W)/totalLen); 
   
            jTable.doLayout(); 
            jTable.validate();



      //System.err.println("-------------"+j);
 return tab1;
 
 
 
 
}
void inPutListFirst(String [][] tab)
{
         
    mainPaneDown=new JPanel();
 for(int j=0;j<varList.size() ;j++)
{
 ShowVar sv=varList.get(j);   
String [] lin=tab[j];
JList  lineList=new JList();  
//System.err.println("-------------"+lin.length);
lineList.setPreferredSize(new Dimension((int)(((W-100))*sv.LENGTH_VAR)/totalLength,20*lin.length));
  
if(COLOR_FCT.equals("R_FCT"))
 lineList.setBackground(new Color(R_FCT-(j*DIMINUTION_FCT), G_FCT, B_FCT));   
else if(COLOR_FCT.equals("G_FCT"))
lineList.setBackground(new Color(R_FCT, G_FCT-(j*DIMINUTION_FCT), B_FCT));
else if(COLOR_FCT.equals("B_FCT")) 
lineList.setBackground(new Color(R_FCT, G_FCT, B_FCT-(j*DIMINUTION_FCT)));

        
//lineList.setBackground(new Color(250-(j*20), 255, 255)); bleu
lineList.setListData(lin);
linJlist.add(lineList); 
mainPaneDown.add(lineList); 
}

}
void inPutListSecond(String [][] tab)
{ 
 for(int j=0;j<linJlist.size() ;j++)
{
 JList  lineList=linJlist.get(j);   
String [] lin=tab[j];  
lineList.setListData(lin);  
}

}

String setVirguleD(double frw)
        {
    //System.out.println("  bb"+frw);
   StringTokenizer st1=new StringTokenizer (""+frw);

    String entier =st1.nextToken(".");
//System.out.println(frw);
    String decimal=st1.nextToken("").replace(".", "");

    if(decimal.length()==1 && decimal.equals("0") )
    decimal=".00";
    else if (decimal.length()==1 && !decimal.equals("0") )
          decimal="."+decimal+"0";
    else
    decimal="."+decimal.substring(0, 2);

String setString = entier+decimal;

int l =setString.length();
if(l<2)
   setString = "    "+frw;
else if(l<3)
   setString = "  "+frw;
else if(l<4)
   setString = "  "+frw;

int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
    setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;
}
return setString;
}
String setVirgule(int frw)
        {

String setString = ""+frw;
int l =setString.length();
if(l<2)
   setString = "    "+frw;
else if(l<3)
   setString = "  "+frw;
else if(l<4)
   setString = "  "+frw;


if(l>3 && l<=6)
setString= setString.substring(0,l-3)+","+setString.substring(l-3) ;

if(l>6&& l<=9)
{
    
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+","+s1.substring(sl-3)+","+setString.substring(l-3)  ;
}
if(l>9)
{ 
//setString=setString.substring(0,l-9)+","+setString.substring(0,l-9)+","+setString.substring(l-6)+","+setString.substring(l-3)  ;
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l-3)+","+setString.substring(l-3,l)  ;
}


       return setString;
}
 public static void main(String[] arghs) 
    {  
        
    }
    
    
    

}
