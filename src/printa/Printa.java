/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package printa;

import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import javax.swing.JOptionPane; 

/**
 *
 * @author Kimenyi
 */
public class Printa {
static int compteur=0;
    String driver = "org.apache.derby.jdbc.NetWorkDriver";
// thekim database name  
String dbName;  
String server;
String connectionURL ;
ResultSet outResult, outResult1;
Connection conn = null;
Statement s,s1;
LinkedList<Lot>lot = new LinkedList();
PreparedStatement psInsert;
PreparedStatement psInsert1;
String  datez,mois,observa,ip;
 LinkedList <String>controlInvoice ; 
LinkedList <Lot> lotList ;
String devise="FBU"; 
    
Printa(String server,String dbName)    
{
this.server=server;
this.dbName=dbName;
connectionURL = "jdbc:derby://"+server+":1527/" + dbName + ";create=false";
read();
}
public  void read()
{
 
    try {
    conn = DriverManager.getConnection(connectionURL);		 
    s = conn.createStatement();
    //s1 = conn.createStatement();
     System.out.println("connected to  "+connectionURL)  ;
    }
    catch (Throwable e){
        System.out.println("errrr to  "+connectionURL)  ; 
    }
}
     
public void codebar()
{  
    
String choix =(String)JOptionPane.showInputDialog(null, "IMPRESSION", "CODEBAR",
   JOptionPane.QUESTION_MESSAGE, null, new String [] {"ARTICLE","BLT","BLT ARTICLE","AU CHOIX","LIVRAISON UNIPHARMA","ALL"}, "");
    if(choix.equals("ALL"))
    {
    try {
    LinkedList <Lot> codebarList=new LinkedList();
    LinkedList <Lot> codebarListDecale=new LinkedList();
    String line="select product.id_product,id_lots,date_exp,name_product,qtecmd,qty_live from APP.LOT,PRODUCT where   PRODUCT.ID_PRODUCT=LOT.ID_PRODUCT order by date_exp,id_lot ";
   // System.out.println(line);
    outResult = s.executeQuery(line);
    while (outResult.next()) {

    int productCode = outResult.getInt(1);
    String id_LotS = outResult.getString(2);
    String date1   = outResult.getString(3);
    String name    = outResult.getString(4);
    int qtecmd     = outResult.getInt(5);
    
    if(qtecmd==0)
     qtecmd=1;
    
    int qtelive = outResult.getInt(6);
   // System.out.println(qtelive+"  "+qtecmd);
    int mod =(qtelive/qtecmd)+1;
   // System.out.println(name+"  "+mod);
    if(qtecmd!=1)
    for(int i=0;i<mod;i++)
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
    else
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
    }
    System.out.println("");
    for(int i =1; i < codebarList.size();i++)
    {
    Lot lotNorma=codebarList.get(i-1);
    System.out.println(lotNorma);
    Lot lotDecale=codebarList.get(i);
    System.out.println(lotDecale);
   // lotDecale.proname=lotNorma.proname;
    codebarListDecale.add(lotDecale);
    //a
    //b a
    //c
    } 
    
    new PrintCodeBar(codebarList);

    } catch (Throwable ex) {
    JOptionPane.showMessageDialog(null,"Problem "+ex,"Downloading codebar",JOptionPane.PLAIN_MESSAGE);
    }

    }
else if(choix.equals("LIVRAISON UNIPHARMA"))
{
try {
LinkedList <Lot> codebarList=new LinkedList();

/*for(int y=0; y<100; y++)
{Lot LL =  new Lot("","","","","","","","","","",""); 
//System.out.println(LL); 
codebarList.add(LL); 
}*/
String num = JOptionPane.showInputDialog(null, " Numéro LIVRAISON","1265");
String line="select product.CODE,NUM_LOT,date_exp,name_product,QUANTITE from APP.LIST,PRODUCT where  ID_INVOICE=" + num + " AND PRODUCT.CODE=CODE_UNI order by NAME_PRODUCT ";
System.out.println(line);

outResult = s.executeQuery(line);
 
while (outResult.next()) {
String productCode = outResult.getString(1);
String id_LotS  = outResult.getString(2);
String date1    = outResult.getString(3);
String name     = outResult.getString(4);
int qte         = outResult.getInt(5);

System.out.println(name+" "+qte);
if(qte<40)
for(int y=0; y<qte; y++)
{
Lot LL =  new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""); 
//System.out.println(LL); 
codebarList.add(LL);
}
else
codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","","")); 
//codebarList.add(new Lot(name,"FIN",productCode,"","","","","","","","")); 
}

/*for(int i=0;i<codebarList.size();i=i+146)
    if((i+146) < codebarList.size())
new unipharma2D(new LinkedList(codebarList.subList(i, i+146))); 
else
new unipharma2D(new LinkedList(codebarList.subList(i,codebarList.size()))); 
    */   
new unipharma2D(codebarList);

} catch (Throwable ex) {
   JOptionPane.showMessageDialog(null,"Problem "+ex,"shida codebar",JOptionPane.PLAIN_MESSAGE);
}
}
else if(choix.equals("ARTICLE"))
{
try {
LinkedList <Lot> codebarList=new LinkedList();

    String num = JOptionPane.showInputDialog(null, " ID ARTICLE","312");
    String line="select product.id_product,id_lots,date_exp,name_product,qtecmd,qty_live from APP.LOT,PRODUCT where  LOT.ID_PRODUCT= " + num + " AND PRODUCT.ID_PRODUCT=LOT.ID_PRODUCT order by date_exp,id_lot ";
    ///System.out.println(line);

   outResult = s.executeQuery(line);
while (outResult.next()) {

int productCode = outResult.getInt(1);
String id_LotS = outResult.getString(2);
String date1 = outResult.getString(3);
String name = outResult.getString(4); 
int qtecmd   = outResult.getInt(5);
    if(qtecmd==0)
    qtecmd=1;
    int qtelive = outResult.getInt(6);
    //System.out.println(qtelive+"  "+qtecmd);
    int mod =(qtelive/qtecmd)+1;
   // System.out.println(name+"  "+mod);
    if(qtecmd!=1&& mod<30)
   for(int i=0;i<mod;i++)
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
   else
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
    }

  LinkedList <Lot> codebarListDecale=new LinkedList();
  
//System.out.println("ssssssssssssssssssssssssssssssss"+codebarList.size());
 
  new PrintCodeBar(codebarList);
   
} catch (Throwable ex) {
   JOptionPane.showMessageDialog(null,"Problem "+ex,"Downloading codebar",JOptionPane.PLAIN_MESSAGE);
} 
}
else if(choix.equals("BLT"))
{


try {
LinkedList <Lot> codebarList=new LinkedList();
 
     String blt = JOptionPane.showInputDialog(null, " ID BLT");
    String line="select product.id_product,lotcmd,expcmd,name_product,qtecmd,quantite from APP.cmd,PRODUCT where  cmd.id_bon_cmd= " + blt + "   AND PRODUCT.ID_PRODUCT=cmd.ID_PRODUCT  ";
    System.out.println(line);

   outResult = s.executeQuery(line);
while (outResult.next()) {

int productCode = outResult.getInt(1);
String id_LotS = outResult.getString(2);
String date1 = outResult.getString(3);
String name = outResult.getString(4); 
int qtecmd   = outResult.getInt(5);
    if(qtecmd==0)
    qtecmd=1;
    int qtelive = outResult.getInt(6);
    System.out.println(qtelive+"  "+qtecmd);
    int mod =(qtelive/qtecmd)+1;
    System.out.println(name+"  "+mod);
    if(qtecmd!=1&& mod<30)
   for(int i=0;i<mod;i++)
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
   else
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
    }

  LinkedList <Lot> codebarListDecale=new LinkedList();
System.out.println("ssssssssssssssssssssssssssssssss"+codebarList.size());
 
  new PrintCodeBar(codebarList);
   
} catch (Throwable ex) {
   JOptionPane.showMessageDialog(null,"Problem "+ex,"Downloading codebar",JOptionPane.PLAIN_MESSAGE);
} 
}
else if(choix.equals("BLT ARTICLE"))
{


try {
LinkedList <Lot> codebarList=new LinkedList();

    String num = JOptionPane.showInputDialog(null, " ID ARTICLE");
     String blt = JOptionPane.showInputDialog(null, " ID BLT");
    String line="select product.id_product,lotcmd,expcmd,name_product,qtecmd,quantite from APP.cmd,PRODUCT where  cmd.id_bon_cmd= " + blt + " and cmd.ID_PRODUCT= " + num + " AND PRODUCT.ID_PRODUCT=cmd.ID_PRODUCT  ";
    System.out.println(line);

   outResult = s.executeQuery(line);
while (outResult.next()) {

int productCode = outResult.getInt(1);
String id_LotS = outResult.getString(2);
String date1 = outResult.getString(3);
String name = outResult.getString(4); 
int qtecmd   = outResult.getInt(5);
    if(qtecmd==0)
    qtecmd=1;
    int qtelive = outResult.getInt(6);
    System.out.println(qtelive+"  "+qtecmd);
    int mod =(qtelive/qtecmd)+1;
    System.out.println(name+"  "+mod);
    if(qtecmd!=1&& mod<30)
   for(int i=0;i<mod;i++)
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
   else
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
    }

  LinkedList <Lot> codebarListDecale=new LinkedList();
System.out.println("ssssssssssssssssssssssssssssssss"+codebarList.size());
 
  new PrintCodeBar(codebarList);
   
} catch (Throwable ex) {
   JOptionPane.showMessageDialog(null,"Problem "+ex,"Downloading codebar",JOptionPane.PLAIN_MESSAGE);
} 
}

else if(choix.equals("AU CHOIX"))
{
try {
LinkedList <Lot> codebarList=new LinkedList();

    String num = JOptionPane.showInputDialog(null, " Numéro LIVRAISON");

    String line="select product.id_product,id_lots,date_exp,name_product,qtecmd,qty_live from APP.LOT,PRODUCT where  bon_livraison='BLA0" + num + "' AND PRODUCT.ID_PRODUCT=LOT.ID_PRODUCT order by name_product,date_exp,id_lot ";

    System.out.println(line);

   outResult = s.executeQuery(line);

System.out.println("select * from APP.LOT,PRODUCT where  bon_livraison='BLA0" + num + "' AND PRODUCT.ID_PRODUCT=LOT.ID_PRODUCT order by date_exp,id_lot ");
while (outResult.next()) {

int productCode = outResult.getInt(1);
String id_LotS = outResult.getString(2);
String date1 = outResult.getString(3);
String name = outResult.getString(4);
System.out.println(name);
int qtecmd   = outResult.getInt(5);
    if(qtecmd==0)
    qtecmd=1;
    int qtelive = outResult.getInt(6);
    System.out.println(qtelive+"  "+qtecmd);
    int mod =(qtelive/qtecmd)+1;
    System.out.println(name+"  "+mod);
    if(qtecmd!=1&& mod<30)
   for(int i=0;i<mod;i++)
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
   else
    codebarList.add(new Lot(name,""+productCode,id_LotS,date1,"","","","","","",""));
    }

  LinkedList <Lot> codebarListDecale=new LinkedList();
System.out.println("ssssssssssssssssssssssssssssssss"+codebarList.size());
 
  new PrintCodeBar(codebarList);

} catch (Throwable ex) {
   JOptionPane.showMessageDialog(null,"Problem "+ex,"Downloading codebar",JOptionPane.PLAIN_MESSAGE);
}
} 
}
    /**
     * @param args the command line arguments
     */
   public static void main (String[] args)
   {
      // Create an object that will hold all print parameters, such as
      // page size, printer resolution. In addition, it manages the print
      // process (job). 
      
       
}

}
