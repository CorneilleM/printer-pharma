/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */ 
package printa;
import java.awt.*; 
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.StringTokenizer; 
import javax.swing.JFrame; 
import javax.swing.JOptionPane;
/**
 *
 * @author Kimenyi
 */
public class Print extends JFrame {

LinkedList <Lot> lotList;
int pageNum = 0;
private Properties properties = new Properties();
int margin=0;
int numero;
double total,tva;
Frss frss;
String date,reference,mode;
String [] footer;
AutoOut db;
String facture="";
Print(LinkedList <Lot> lotList,AutoOut db,String mode ,String date,Frss frss,int numero,int tva,double total,String [] footer)
{
//    System.out.println("wewe   "+footer[1]);
this.lotList=lotList;
this.db=db;
this.mode=mode;
this.margin=getValue("margin");
this.date=date;
this.frss=frss;
this.numero=numero;
this.tva=tva;
this.total=total;
this.footer=footer;
PrintCommand();

}
private void PrintCommand()
{
      
      facture="FACTURE GLOBALE : "+getString("factureV")+(numero);
          System.out.println();  
    PrintJob pjob = getToolkit().getPrintJob(this,frss.NOM_CLIENT+facture, properties);
    
   // PageFormat documentPageFormat = new PageFormat(200,500);
 
    if (pjob != null) {
    Graphics pg = pjob.getGraphics();
    if (pg != null) {
       

    printCommande(pjob, pg);
    pg.dispose();

    
    }
    pjob.end();
    }
}
private void printCommande (PrintJob pjob, Graphics pg) {



   if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }

    int pageH = pjob.getPageDimension().height - margin;
    int pageW = pjob.getPageDimension().width - margin;
     //System.out.println(pageH+" getPageDimension "+pageW);
   
  if (pg != null)
  {
    BufferedInputStream bis = null;
    try {
    Font helv = new Font("Helvetica", Font.BOLD, getValue("dateT"));
    pg.setFont(helv);
    System.out.println("mode:"+mode+"LOGO:"+getString("logoV"));
    Image img=null;
    bis = new BufferedInputStream(new FileInputStream(getString("logoV")));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
    int ch;
    while ((ch = bis.read()) != -1) {
    baos.write(ch);
    System.err.println(ch);
    }
    System.err.println(img);
    img = Toolkit.getDefaultToolkit().createImage(baos.toByteArray());
     System.err.println(img);
    System.err.println(img.getWidth(rootPane));
    } catch (IOException exception) {
    System.err.println("Error loading: ");
    }
    pg.drawString(getString("ville")+", le " + date.substring(0, 2) + "/" + date.substring(2, 4) + "/" + date.substring(4, 6),  getValue("dateX"), getValue("dateY"));
    helv = new Font("Helvetica", Font.BOLD, 25);
    pg.setFont(helv); 
    
    Thread t = new Thread();
    t.start();
    t.sleep(3000);
    pg.drawImage(img, getValue("logoX"), getValue("logoY"), getValue("marginPrix"), getValue("longeurPrix"), rootPane);

    helv = new Font("Helvetica", Font.PLAIN, getValue("adresseT"));
    //have to set the font to get any output
    pg.setFont(helv);
    FontMetrics fm = pg.getFontMetrics(helv);
    int fontHeight = fm.getHeight();
    int fontDescent = fm.getDescent();

    pg.drawString(getString("adresseV"), getValue("adresseX"),  getValue("adresseY"));
    pg.drawString(getString("rueV"), getValue("rueX"),  getValue("rueY"));
    pg.drawString(getString("localiteV"), getValue("localiteX"),  getValue("localiteY"));
    pg.drawString(getString("telV"), getValue("telX"), getValue("telY"));
    pg.drawString(getString("faxV"), getValue("faxX"), getValue("faxY"));
    pg.drawString(getString("emailV"), getValue("emailX"), getValue("emailY"));
    pg.drawString(getString("tvaV"), getValue("tvaX"),  getValue("tvaY"));
    
     Font helv2 = new Font("Helvetica", Font.BOLD,45);
    pg.setFont(helv2);
    
    pg.drawRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), getValue("clientLargeur"));
     helv2 = new Font("Helvetica", Font.BOLD, getValue("clientT"));
    pg.setFont(helv2);

    
    pg.drawString(frss.NOM_CLIENT, getValue("clientX") + 10,  getValue("clientY") + 10);
    helv2 = new Font("Helvetica", Font.ITALIC, getValue("clientT")-2);
    pg.setFont(helv2);
    pg.drawString(frss.PRENOM_CLIENT, getValue("clientX" ) + 10, getValue("clientY" ) + 20);
    pg.drawString(frss.ln2, getValue("clientX" ) + 10,  getValue("clientY" ) + 30);
    pg.drawString(frss.ln3, getValue("clientX" ) + 10, getValue("clientY" ) + 40);
    pg.drawString(frss.SECTEUR, getValue("clientX" ) + 10,  getValue("clientY" ) + 50);
    Font helv1 = new Font("Helvetica", Font.BOLD, getValue("factureT")); 
            // facture=getString("factureV") + (numero);
    pg.setFont(helv1);
     pg.drawString(facture, getValue("factureX"), getValue("factureY"));

    helv2 = new Font("Helvetica", Font.BOLD, getValue("referenceT"));
    pg.setFont(helv2);
   // pg.drawString(getString("referenceV") + reference, getValue("referenceX"), getValue("referenceY"));
    
    System.out.println(" list1:"+lotList.size());
    System.out.println("lot list1:"+lotList.getFirst().u1+" last"+lotList.getLast().u1);
    pg.drawString("   Reference # "+lotList.getFirst().u1+"-"+lotList.getLast().u1, getValue("referenceX"), getValue("referenceY") );
    pg.drawString("   Nombre de Bons # "+lotList.size(), getValue("referenceX"),  getValue("referenceY")+15);

    helv1 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv1);
    pageNum++;
    pg.drawString("Page  " + pageNum, pageW / 2, pageH - 9); 
    helv2 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv2);
            int g = getValue("length");
            String[] entete = new String [g];
            int[] longeur = new int[g+1];
            String[] variable= new String [g];
            int [] dimension=new int[g];
            longeur[0]=0;

            int page=1;
            int y=  getValue("entete"+page+"X");
            for(int k=0;k<g;k++)
            {
            entete[k]=getString("entete_"+(k+1)+"Valeur");
            longeur[k+1]=getValue("entete_"+(k+1)+"Largeur");
          
            variable[k]=getString("entete_"+(k+1)+"Variable");
            dimension[k]=getValue("entete_"+(k+1)+"Dimension");
 // System.out.println(" d   "   +getValue("entete_"+(k+1)+"Dimension"));
            }

            int w = margin;

            int curHeightLigne = getValue("ligne"+page+"X");

            Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
            pg.setFont(helvLot);
            FontMetrics fmLot = pg.getFontMetrics(helvLot);
            fontHeight = fmLot.getHeight();
            boolean done = true;
     
for (int j = 0; j < lotList.size(); j++)
{

            helvLot = new Font("Helvetica", Font.PLAIN, getValue("tailleLigne"));
            pg.setFont(helvLot);
            Lot ci = lotList.get(j);
          
            w = margin;

            if (done) {
            curHeightLigne += fontHeight;
            } else {
            curHeightLigne += 2 * fontHeight;
            }
              System.out.println(curHeightLigne+" --  "+fontHeight);
            done = true;
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            String valeur=ci.vars[i];

            String variab=variable[i];
            int dim      =dimension[i];
          // System.out.println(variab+" dim  "+valeur); 
            
            
            if(variab.equals("int"))
                printInt(pg,setVirgule(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);
            else  if(variab.equals("double"))
                printInt(pg,setVirguleD(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);
            else  if(variab.equals("centreInt"))
                printIntCentre(pg,setVirguleD(valeur), w + (longeur[i + 1]) , curHeightLigne,longeur[i + 1]);
            else  if(variab.equals("centreS"))
                printStringCentre(pg,valeur , w   , curHeightLigne,longeur[i + 1]);
            
            else if (valeur.length() < dim)
            {
            pg.drawString(valeur, w + 5, curHeightLigne);
            }
            else
            {
            done = false;
            pg.drawString(valeur.substring(0, dim - 1), w + 5, curHeightLigne);
            pg.drawString(valeur.substring(dim - 1, valeur.length()), w + 5, fontHeight + curHeightLigne);
            }
            }

          //  System.out.println(j+" jjjjj  "+curHeightLigne+ "   "+( getValue("longeurLigne")));


            int hauteur = pageH - getValue("marginfooter");

            if ((curHeightLigne+20) >  hauteur-20) {

            /////////////////////////////////////////////////////////////////////////////////////////////
          
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
   
           //  facture=getString("factureV") + (numero);
            pg.drawString(facture, margin, margin + 25);
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
          pg.drawString("Page  " + pageNum, pageW / 2, pageH - 10);
              curHeightLigne=getValue("entete"+page+"X")+25;
              y=getValue("entete"+page+"X");
            done = true;
            }
 } 
///////////////////////////////////////////////////////////////////////////////////////////////////////////

            if((curHeightLigne+20) > ( pageH -getValue("longeurLigne")) )
            {
                 int hauteur = pageH - getValue("marginfooter");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
        // System.out.println(facture+ getValue("factureX")+getValue("factureY"));
   
          pg.drawString(facture, margin, margin + 25);

            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2, pageH - 10);
              curHeightLigne=getValue("entete"+page+"X")+25;
            done = true;
            hauteur = pageH - getValue("longeurLigne");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete

            }
            else
            {

            int hauteur = pageH - getValue("longeurLigne");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete
            }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        w = margin;
                        helv2 = new Font("Helvetica", Font.BOLD, getValue("prixTaille"));
                        pg.setFont(helv2);
                        int lp = getValue("lengthPrix");
                        String[] enteteP = new String [lp+1];
                        int[] longeurP = new int[lp+1];
                        String[] variableP= new String [lp];
                        longeurP[0]=0;



                        for(int k=0;k<lp;k++)
                        {
                        
                        enteteP[k]=getString("prix_"+(k+1)+"Valeur");
                        if(enteteP[k].length()>2)
                        enteteP[k]=enteteP[k]+frss.devise;
                        longeurP[k+1]=getValue("prix_"+(k+1)+"Largeur");
                        variableP[k]=getString("prix_"+(k+1)+"Variable");
                       // System.out.println(longeurP[k+1]);
                        }
                        int Xprix = getValue("longeurY");
                        int marginP=getValue("longeurX");
                        w =marginP ;
                        for (int i = 0; i < lp; i++) {
                        w += (longeurP[i]);
                        
                        //System.out.println(( w + (longeurP[i + 1]) - 40)+"  kkk "+(Xprix+25)+"  total "+total+"   tva "+tva);

                        int wid=w + (longeurP[i + 1]) - 20;
                        if(i==0)
                        printInt(pg,setVirguleD( ""+(int)(total-tva)),  wid, Xprix+15);
                        if(i==1)
                        printInt(pg,setVirguleD(""+ (int)tva),  wid, Xprix+15);
                        if(i==2)
                        printInt(pg,setVirguleD( ""+(int)(total)),  wid, Xprix+15);

                        
                        pg.drawLine(w,Xprix-20, w, Xprix+20);//ligne zihagaze
                        //System.out.println( w +" x1 y1 "+(Xprix-20)+"  y2 "+ Xprix+20);
                        pg.drawString(enteteP[i], w + 2, Xprix - 5);// amazina
                        //System.out.println(enteteP[i]+"entete   x=w+2   yxprix-5= "+(Xprix - 5));
//+frss.devise
                        }
                         if(lp!=0)
                        {
                        pg.drawLine(marginP, Xprix, w, Xprix);//umurongo wo hasi
                       // System.out.println(marginP+"marginP  lineMidlle y=xprix   y= "+w);
                        pg.drawRect(marginP, Xprix-20 , w-marginP , 40); // cadre ya entete
                       //  System.out.println(marginP+" x   cadre y Xprix-20"+( Xprix-20)+" length= "+(w-marginP));

                        }
                         
                           helv2 = new Font("Helvetica", Font.BOLD, 8); 
                       
                       //if(mode.equals("COMMANDE"))
                        pg.setFont(helv2);
                                 
                       if(mode.equals("CALCUL"))
                                     for(int v=1;v<getValue("lengthfooter") && footer.length>v;v++)
                                     {
                                   //      System.out.println( getValue("footer_"+v+"X" )+" dedede "+footer [v]+getValue("footer_"+v+"Y" ));
                                         pg.drawString( footer [v] , getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));
                                     }
                                   else
                                   for(int v=1;v<getValue("lengthfooter") ;v++)
                                        if(v<6)
                                        {
                                            // System.out.println( getString("footer_"+v+"Valeur"));
                                            pg.drawString(getString("footer_"+v+"Valeur")+  ""+footer[v], getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));
                                        }
                                        else
                                        {
                                              // System.out.println( getString("footer_"+v+"Valeur"));
                                            pg.drawString(getString("footer_"+v+"Valeur"), getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));
                                        }

 //pg.drawRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), getValue("clientLargeur"));
   
    } catch (InterruptedException ex) {
                System.out.println(ex);
            } catch (FileNotFoundException ex) {
                System.out.println(ex);
            }
           finally {
                try {
                    bis.close();
                } catch (IOException ex) {
                     System.out.println(ex);
                }
            }

  }

}

int getValue(String s)
{

int ret=0;
Variable var= db.getParam("PRINT",mode,s+mode);
 if(var!=null)
 {
Double val=new Double (var.value);
ret = val.intValue();
}

return ret;
}
String getString(String s)
{
String ret=" ";

 Variable var= db.getParam("PRINT",mode,s+mode);
 if(var!=null)
     ret=var.value;
     return ret;
}
public void printInt(Graphics pg,int nbre,int w,int h)
  {
  String s=setVirgule(nbre);

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;

      pg.drawString(""+s.charAt(i),w-back,h); 
  }
}
public void printInt(Graphics pg,String s,int w,int h)
{
int back=0; 
for(int i=s.length()-1;i>=0;i--)
{
if(s.charAt(i)==' ')
back+=2;
else
back+=5; 
pg.drawString(""+s.charAt(i),w-back,h); 
} 
} 
public void printIntCentre(Graphics pg,String s,int w,int h,int length)
  {

int back=0;
int center=w-(length/2)+((6*s.length())/2);
  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=6;
      pg.drawString(""+s.charAt(i),center-back,h);
      //System.out.print (s.charAt(i)+" - "+(center-back));
  }
}
public void printStringCentre(Graphics pg,String s,int w,int h,int length)
  {
int back=0;
  int f=(length-s.length())/2;
  int center=w+f-16;
  
 // System.out.println(w+"   "+s+"   "+f+"   "+length);
     
for(int i=0;i<s.length();i++)
  {
      pg.drawString(""+s.charAt(i),center+back,h);
      //System.out.print (s.charAt(i)+" - "+(center+back));
      if(s.charAt(i)=='I')
       back+=3;
      else  if(s.charAt(i)=='W' || s.charAt(i)=='M')
       back+=8;
      else
       back+=6;

    
  }

}
 String setVirgule(int frw)
{
String setString = ""+frw;
int l =setString.length();
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3);
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;
}
return setString;
}
String setVirgule(String setString)
{
int l =setString.length();
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;
}
return setString;
} 

String setVirguleD(String frw)
{
 //   System.out.println("**********before********"+frw);
    String setString = frw;
    if(frw.contains("."))
    {
    try
    {
     double d=Double.parseDouble(frw)  ; 
     
        d =((int)(d*1000))/1000.0;
        frw=""+frw;
        // System.out.println("********after**********"+frw);
StringTokenizer st1=new StringTokenizer (frw);
String entier =st1.nextToken(".");
//System.out.println(frw);
String decimal=st1.nextToken("").replace(".", "");
if(decimal.length()==1 && decimal.equals("0") )
decimal=".00";
else if (decimal.length()==1 && !decimal.equals("0") )
decimal="."+decimal+"0";
else
decimal="."+decimal.substring(0, 2);
  setString = entier+decimal;
    }
    catch(Throwable e)
    {System.out.println(frw+e);}
    

int l =setString.length();
if(l<2)
setString = "    "+frw;
else if(l<3)
setString = "  "+frw;
else if(l<4)
setString = "  "+frw;
int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;
}
}
else
        return setVirgule(frw);
    
return setString;
}
public static void main(String[] arghs)
{
    //new PrintDoc();
}




}
