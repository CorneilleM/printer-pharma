package printa;

import java.util.LinkedList;
import java.util.StringTokenizer;

/* Product for Ishyiga copyright 2007 Kimenyi Aimable
*/

class Product implements  Comparable
{

	int productCode,qty;
	String productName,code,famille;
        Lot courrant;
        String codeBar,observation,codeSoc,designation_frss,code_frss,FRSS_PRINCIPALE;
        double tva,coef;
         int quantity,maximum,droit ;
        int choixLot=1; 
        int qtyLive; 
        double currentPrice,prix_revient,poids,volume,qtecmd,contient,cond;
        String code1,code2,code3,code4,devise;

Product(int productCode,String productName,String code,double currentPrice,double tva,String observation,String codeBar,
      double prix_revient,double coef ,String famille, int maximum,String code_frss,String designation_frss,double cond, String FRSS_PRINCIPALE)
{
this.productCode=productCode;
this.productName=productName;
this.code=code;
this.currentPrice=currentPrice; 
this.tva=tva;
this.observation=observation;
this.codeBar=codeBar;
this.prix_revient=prix_revient;
this.coef=coef;
this.famille=famille;
this.maximum=maximum;
this.code_frss=code_frss;
this.designation_frss=designation_frss;
this.cond=cond;
this.FRSS_PRINCIPALE=FRSS_PRINCIPALE;



}
Product(int productCode,String productName,String code,int qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc,String des_frss,String code_frss,double cond,String famille,double coef,String FRSS_PRINCIPALE)
{
		this.productCode=productCode;
		this.productName=productName;
                      //  if(productName.length()>55)
                   //this.productName=productName.substring(0, 50);         
                this.code=code;
                this.maximum =qty;
                this.currentPrice=currentPrice; 
                this.tva=tva;
                this.codeBar=codeBar;
                this.observation=observation;
                this.codeSoc=codeSoc;
                this.cond=cond;
                this.designation_frss=des_frss;
                this.code_frss=code_frss;
                this.famille=famille;
                this.coef=coef;
                this.FRSS_PRINCIPALE=FRSS_PRINCIPALE;
}

Product(int productCode,String productName,String code,int qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc,String des_frss,String code_frss,double cond,String famille,double coef,String FRSS_PRINCIPALE,double poids,double volume,double qtecmd,double contient,int droit)
{
		this.productCode=productCode;
		this.productName=productName;
                    //    if(productName.length()>55)
                   //this.productName=productName.substring(0, 50);
                this.code=code;
                this.maximum =qty;
                this.currentPrice=currentPrice; 
                this.tva=tva;
                this.codeBar=codeBar;
                this.observation=observation;
                this.codeSoc=codeSoc;
                this.cond=cond;
                this.designation_frss=des_frss;
                this.code_frss=code_frss;
                this.famille=famille;
                this.coef=coef;
                this.FRSS_PRINCIPALE=FRSS_PRINCIPALE;
                this.poids=poids;
                this.volume=volume;
                this.contient=contient;
                this.qtecmd=qtecmd;
                this.droit=droit;


}
Product(int productCode,String productName,String code,int qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc,int qte,String des_frss,String code_frss,double cond,String famille,double coef)
{
		this.productCode=productCode;
		this.productName=productName;
                        if(productName.length()>55)
                   this.productName=productName.substring(0, 50);
                this.code=code;
                this.maximum =qty;
                this.currentPrice=currentPrice;
                this.qty=qte;
                this.tva=tva;
                this.codeBar=codeBar;
                this.observation=observation;
                this.codeSoc=codeSoc;
                this.cond=cond;
                this.designation_frss=des_frss;
                this.code_frss=code_frss;
                this.famille=famille;
                this.coef=coef;
}
 Product(int productCode,String productName,String code,int qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc)
{
		this.productCode=productCode;
		this.productName=productName;
                      //  if(productName.length()>55)
                   //this.productName=productName.substring(0, 50);
                this.code=code;
                this.qty =qty;
                this.currentPrice=currentPrice; 
                this.tva=tva;
                this.codeBar=codeBar;
                this.observation=observation;
                this.codeSoc=codeSoc;
}
Product(int productCode,String productName,String code,int qty ,double currentPrice,double tva,String codeBar,String observation,String codeSoc, int max,String famille,double coef)
{
		this.productCode=productCode;
		this.productName=productName;
                   //     if(productName.length()>55)
                  // this.productName=productName.substring(0, 50);         
                this.code=code;
                this.qty =qty;
                this.currentPrice=currentPrice; 
                this.tva=tva;
                this.codeBar=codeBar;
                this.observation=observation;
                this.codeSoc=codeSoc;
                this.maximum=max;
                this.famille=famille;
                this.coef=coef;
                
}


public int qty ()
{
		return qty;
}
/* gestion de vente */

public String toPrint()
{
    String nom="";
    
  if(productName.length()>20)
  nom=productName.substring(0,20)+".";
  else 
     nom=productName;
    
    int i = nom.length();
          
  while(i<=20)
  {
  nom=nom+" ";
  i++;
  }

return nom+" * "+qty+" = "+currentPrice*qty+" rwf";
}


// Qte Live
public int  getQuantite()
{
    int g =0;
    
    return g;
}

/* gestion du stock de ce produit voir la qty */
public boolean check(int i)
{
    
    qtyLive =getQuantite();
	boolean done=false;
	 if( qtyLive >= i)
	 		{
	 			done=true;
	 		} 
        
return done;
}
public double salePrice()
{
	return currentPrice*qty;
}
public int productCode()
{
	return productCode;
}

    @Override
public String toString()
{
   
return productName+"****"+setVirgule(currentPrice)+" ";
}
    
public String toSell()
{
  String g ="****"+ codeSoc;
return productCode+"****"+qty+"****"+(int)(currentPrice*qty)+" "+"****"+productName+g;
}
public String toStock()
{
   // getQuantite();
return productCode+"***"+productName;//+"****"+courrant.id_LotS+"****"+courrant.qtyLive;
}
public String toSee()
{
    return productName+"****"+setVirgule((int)currentPrice)+" ";
}

public Product clone(int q)
{
    
Product p= new Product(productCode,productName, code, qty,currentPrice,tva,codeBar,observation,codeSoc );
p.qty=q;

return p;

}
String setVirgule(double frw)
        {
   StringTokenizer st1=new StringTokenizer (""+frw);

    String entier =st1.nextToken(".");

    String decimal=st1.nextToken("").replace(".", "");

    if(decimal.length()==1 && decimal.equals("0") )
    decimal=".00";
    else if (decimal.length()==1 && !decimal.equals("0") )
          decimal="."+decimal+"0";
    else
    decimal="."+decimal.substring(0, 2);

String setString = entier+decimal;

int l =setString.length();
if(l<2)
   setString = "    "+frw;
else if(l<3)
   setString = "  "+frw;
else if(l<4)
   setString = "  "+frw;

int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
    setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;

}

return setString;
}

public Product cloneRet(int q)
{
Product p= new Product(productCode,productName, code, qty,currentPrice,tva,codeBar,observation,codeSoc );
p.qty=-q;

return p;

}
public int compareTo( Object o )
    {
        if( o instanceof Product ){
           Product r = (Product)o; 
           return this.productName.compareTo( r.productName );
        }
        return -1;
    }

}