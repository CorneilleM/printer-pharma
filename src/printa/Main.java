/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */ 
package printa;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.sql.*;
import java.util.*; 
import java.util.Date;
import java.util.logging.*;
import javax.swing.*;

/**
 *
 * @author Kimenyi
 */
public class Main extends JFrame {
  int LENTH=400;
  int WIDT =300;
LinkedList <Lot> lotList;
ResultSet outResult;
Connection conn = null;
Statement s;
LinkedList<Credit>credit = new LinkedList();
JList  societeJList=new JList();
JList  formatPrintJList=new JList();
int soldeClient=0;
int CashClient=0;
int tvaYose=0;
AutoOut db;
AutoOut db2;
AutoOut db3;
AutoOut db4;
AutoOut db5;
AutoOut db6;
Assurance ass;
Frss frss;
String mode;
String d;
int numero; 
String server, database;

JButton printIshyigaButtton,previewIshyigaButtton,mapIshyigaButtton,assuranceIshyigaButtton;

     Main (String server,String database,String mode, String lesDb )
    { 
        
   this.db= new AutoOut(server,database);
//   this.db2=new AutoOut(server, "KCT");
//   this.db3=new AutoOut(server, "bridge");
//   this.db4=new AutoOut(server, "speranza");
//   this.db5=new AutoOut(server, "UNIPHARMAB2");
//   this.db6=new AutoOut(server, "UNIPHARMAB4");
//   
    db.read();
//    db2.read(); 
//    db3.read();
//    db4.read();
//    db5.read();
//    db6.read();
    
    if(lesDb.contains("#")){
        LinkedList<AutoOut> lesConnexion = new LinkedList<>();
        String [] entity = lesDb.split("#");
        for(String d : entity){
         lesConnexion.add(new AutoOut(server, d));
        }
        
        switch (entity.length) {
            case 1 -> {
                db2 = lesConnexion.get(0);
                db2.read();
           }
            case 2 -> {
                db2 = lesConnexion.get(0);
                db3 = lesConnexion.get(1);
                db2.read();
                db3.read();
           }
            case 3 -> {
                db2 = lesConnexion.get(0);
                db3 = lesConnexion.get(1);
                db4 = lesConnexion.get(2);
                db2.read();
                db3.read();
                db4.read();
           }
            case 4 -> {
                db2 = lesConnexion.get(0);
                db3 = lesConnexion.get(1);
                db4 = lesConnexion.get(2);
                db5 = lesConnexion.get(3);
                db2.read();
                db3.read();
                db4.read();
                db5.read();
           }
            case 5 -> {
                db2 = lesConnexion.get(0);
                db3 = lesConnexion.get(1);
                db4 = lesConnexion.get(2);
                db5 = lesConnexion.get(3);
                db6 = lesConnexion.get(4);
                db2.read();
                db3.read();
                db4.read();
                db5.read();
                db6.read();
           }
            default -> throw new AssertionError();
        }
    }else{
        JOptionPane.showMessageDialog(null, "Check your setup \n Wrong db's specification");
    }
    
    this.server=server;
    this.database=database;
    this.s=db.s;
    this.conn=db.conn;
    Assurance [] assuranceList= assurance();
    societeJList.setListData(assuranceList);
    societeJList.setSelectedIndex(0) ;
    String [] format=new String []{"LIVRAISON","COMMANDE","CALCUL","FACTURESORAS","FACTURERAMA","MOUVEMENT","FACTURE1","FACTURE","PROFORMADEVISE","NOTECREDIT","CREDITCORAR","RETOUR","PROFORMA","FACTUREDEVISE","LIVRAISONDEVISE"};
    formatPrintJList.setListData(format);
    formatPrintJList.setSelectedIndex(0) ;
    this.mode=mode;
    draw();
    }
public void draw()
{
        setTitle("Ishyiga Printer"); 
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); 
        Container content = getContentPane(); 
        eastPane(content);
        setSize(WIDT,LENTH);
        setVisible(true); 
        
        printIshyiga();
        mapIshyiga();
        previewIshyiga();  
        assuranceIshyiga();
}
 public void eastPane(  Container content )
{
    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(3,2)); 
    
    printIshyigaButtton=new JButton("PRINT");
    printIshyigaButtton.setBackground(Color.green);
    printIshyigaButtton.setFont(new Font("Helvetica", Font.PLAIN, 20));

    previewIshyigaButtton=new JButton("PREVIEW");
    previewIshyigaButtton.setBackground(Color.yellow);
    previewIshyigaButtton.setFont(new Font("Helvetica", Font.PLAIN, 20));

    mapIshyigaButtton= new JButton("MAP");
    mapIshyigaButtton.setBackground(Color.orange);
    mapIshyigaButtton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    assuranceIshyigaButtton= new JButton("Assurance");
    //assuranceIshyigaButtton.setBackground(Color.orange);
    assuranceIshyigaButtton.setFont(new Font("Helvetica", Font.PLAIN, 20));
    
JScrollPane  scrollsocieteJList =new JScrollPane(societeJList);
scrollsocieteJList.setPreferredSize(new Dimension (40,40));
societeJList.setBackground(Color.pink);
 
JScrollPane  scrollformatPrintJList =new JScrollPane(formatPrintJList);
scrollformatPrintJList.setPreferredSize(new Dimension (40,40));
formatPrintJList.setBackground(Color.lightGray);
 
    panel.add( scrollsocieteJList);
    panel.add( scrollformatPrintJList);
    panel.add(printIshyigaButtton);
    panel.add(previewIshyigaButtton);
    panel.add( mapIshyigaButtton); 
    panel.add( assuranceIshyigaButtton); 
    
    content.add(panel, BorderLayout.EAST);
}
   
     public void printIshyiga()
{
	printIshyigaButtton.addActionListener(
		new ActionListener()
	{  public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==printIshyigaButtton)
		{
              
     String choix =(String)JOptionPane.showInputDialog(null, "IMPRESSION", "ISHYIGA",
   JOptionPane.QUESTION_MESSAGE, null, new String [] {"DOCUMENTS","CODEBAR"}, "");
  
     
     if(choix.equals("CODEBAR"))
    {    
       Printa printa =new Printa(server,database); 
       printa.codebar();  
    }  
     if(choix.equals("DOCUMENTS"))
    {ass = (Assurance) societeJList.getSelectedValue();
   
      //System.exit(0);
      String add[]=ass.adresse.split("#");
  //frss=new Frss(  ass.sigle, ass.sigle ,ass.designation,ass.adresse," " ,0,0,"","","","","","",0);
  if(add.length==4){
  frss=new Frss(  ass.sigle, add[0] ,add[1],add[2],add[3] ,0,0,"","","","","","",0);
  }else{
  frss=new Frss(  ass.sigle, ass.sigle ,ass.designation,ass.adresse," " ,0,0,"","","","","","",0);
  }
  mode=(String)formatPrintJList.getSelectedValue();
 
  d  = JOptionPane.showInputDialog(null, " Date de facturation ","310821");  
 int numero=1;
  try
  {
   numero = Integer.parseInt( JOptionPane.showInputDialog(null, " Numero Facture  Umubare ","1")); 
  }
  catch(NumberFormatException e)
  {
      System.err.println("error : " + e);
  } 
  credit=new LinkedList();
 lotList =new LinkedList ();  
 soldeClient=0;
 CashClient=0;
 tvaYose=0;
 
 if(database.contains("ishyiga"))
 {
     getCredit(ass.sigle,db,"U");
     getCredit(ass.sigle,db2,"K");
     getCredit(ass.sigle,db3,"B");
     getCredit(ass.sigle,db4,"S");
     getCredit(ass.sigle,db5,"B2_");
     getCredit(ass.sigle,db6,"B4_");
 }
 else
 {
     getCredit(ass.sigle);     
 } 
  
  new Print(lotList, db,mode, d, frss,numero,tvaYose,soldeClient,new String [] {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""});
                }
                
                }  
}});
} 
     
public void assuranceIshyiga()
{
assuranceIshyigaButtton.addActionListener(
        new ActionListener()
{  public void actionPerformed(ActionEvent ae)
{
        if(ae.getSource()==assuranceIshyigaButtton)
        {

String selected =((Assurance)societeJList.getSelectedValue()).sigle;
String  desi = JOptionPane.showInputDialog("ENTREZ La nouvelle designation de "+selected,((Assurance)societeJList.getSelectedValue()).designation);
String  adresse = JOptionPane.showInputDialog("ENTREZ La nouvelle Adresse de "+selected,((Assurance)societeJList.getSelectedValue()).adresse);
                    try {
                        db.s = db.conn.createStatement();
                        String createString1 = "update APP.VARIABLES set VALUE_variable='"+desi+"',VALUE2_variable='"+adresse+"' where APP.variables.nom_variable = '"+selected+"'";
db.s.execute(createString1);
                    } catch (SQLException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } 
}
}});
} 
    
      public void mapIshyiga()
{
	mapIshyigaButtton.addActionListener(
		new ActionListener()
	{  public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==mapIshyigaButtton)
		{
                
 String mode=(String)formatPrintJList.getSelectedValue(); 
 new Map(db,mode);
                 
                }
        }});
}
      
       public void previewIshyiga()
{
	previewIshyigaButtton.addActionListener(
		new ActionListener()
	{  public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==previewIshyigaButtton)
		{
  ass = (Assurance) societeJList.getSelectedValue();
  frss=new Frss(  ass.sigle, ass.sigle ,ass.designation,ass.adresse," " ,0,0,"","","","","","",0);
  mode=(String)formatPrintJList.getSelectedValue();
  
     String CHOIX = (String) JOptionPane.showInputDialog(null,
                                   ""," ",
                                    JOptionPane.QUESTION_MESSAGE, null,
                                    new String[]{"IMPORT","PHARMA","COMPTA","SYMPHONY","BAR","HOTEL"},
                                    "");
     if(CHOIX.equals("PHARMA"))
     {  d  = JOptionPane.showInputDialog(null, " Date de facturation ","300411");  
         getCredit(ass.sigle,db,"");  
 new PrintSetup(lotList, db,mode, d, frss,numero,tvaYose,soldeClient,new String [] {"","","","","","","","","","","","","","","","","","","","","","","","",""}); 
     }
     else  if(CHOIX.equals("IMPORT"))
     { 
         String ch = (String) JOptionPane.showInputDialog(null, "Fait votre choix", "Impression ",
                                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"PAR HEURE","PAR GROUPE", "PAR NUMERO"}, ""); 
                                 int qtyCode = 0;
                                 if (ch.equals("PAR GROUPE"))                                      
                                 { 
           Invoice inv = (Invoice) JOptionPane.showInputDialog(null, "Fait votre choix", "Impression ",
                                 JOptionPane.QUESTION_MESSAGE, null, db.printByChoice(ch), ""); 
                                     qtyCode=inv.id_invoice;   
                                 }
                                 if (ch.equals("PAR HEURE")) 
                                 {   Invoice inv = (Invoice) JOptionPane.showInputDialog(null, "Fait votre choix", "Impression ",
                                 JOptionPane.QUESTION_MESSAGE, null, db.printByChoice(ch), ""); 
                                     qtyCode=inv.id_invoice; 
                                 }
                                 if (ch.equals("PAR NUMERO")) 
                                 {
                                    qtyCode = getIntVal("Numero Facture à imprimer", " ", 0); 
                                 }  
          db.printRetard(qtyCode,mode);
     }
     
                
                
                
                }
        }});
} 
    
 Assurance []  assurance()
{ 
 LinkedList <Assurance>  ass  =new LinkedList(); 
    try
    {
    outResult = s.executeQuery("select NOM_VARIABLE,value_variable,value2_variable from APP.VARIABLES WHERE FAMILLE_VARIABLE='CLIENT' order by NOM_VARIABLE");

    while (outResult.next())
    {
    ass.add(new Assurance(outResult.getString(1),outResult.getString(2),outResult.getString(3),"",""));
    } 
    outResult.close(); 
    }  catch (Throwable e)  {
    db.insertError(e+""," V ass");
    }
  Assurance []  control=new Assurance [ass.size()];
    for(int g=0;g<ass.size();g++)
    {  
        control[g]=ass.get(g) ; 
    }
   return control;
}
public void  getCredit(String stock,AutoOut d,String cc)
{ 
    try
    {

String dbb="";
String nAff  = (String)JOptionPane.showInputDialog(null, "Fait votre choix", "PRINT EXCEL",
JOptionPane.QUESTION_MESSAGE, null, new String [] {"NORMAL","SPECIAL"}, "");
 

 String  Debut = JOptionPane.showInputDialog("ENTREZ LE TEMPS du debut",db.startTime());
 String FIN = JOptionPane.showInputDialog("ENTREZ LE TEMPS de la fin ",db.EndTime());

if(stock.equals("RAMA"))
dbb="_RAMA";
if(stock.equals("RAMA"))
{ dbb="_RAMA";

d.outResult = d.s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,"
        + "tva,percentage,document,beneficiaire from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE   invoice.HEURE >'"+Debut+"' "
                + "AND invoice.HEURE <'"+FIN+"' and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and "
                        + "num_affiliation=numero_affilie and cause='M' order by INVOICE.ID_INVOICE ");

System.out.println("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document,"
        + "beneficiaire from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE   invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "
                + "and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' "
                        + "order by INVOICE.ID_INVOICE ");

while (d.outResult.next())
{ 
                int  ID_INVOICE= d.outResult.getInt(1);
                String NUMERO_AFFILIE=d.outResult.getString(2);
                String NUMERO_QUITANCE=  d.outResult.getString(3);
                int SOLDE=d.outResult.getInt(4);
                int PAYED = 0;
                if(d.outResult.getInt(10) != 0 && d.outResult.getInt(5) == 0)
                {
                    PAYED = getCashPayed(ID_INVOICE, d.stat);
                }else{
                    PAYED = d.outResult.getInt(5);
                }
                String da =d.outResult.getString(6);
                String nomO =d.outResult.getString(7);
                String prenom =d.outResult.getString(8); 
                int tva =d.outResult.getInt(9);
                int vtva=(int)((100-d.outResult.getInt(10))*tva)/100; 
                String  beneficiaire=d.outResult.getString(12);  
 System.out.println(":"+beneficiaire+"**");
 credit.add(new  Credit( ID_INVOICE, NUMERO_AFFILIE , NUMERO_QUITANCE, SOLDE, PAYED,da,nomO+" "+prenom,beneficiaire,vtva) );
 
 if(beneficiaire.contains("MEME")||beneficiaire.contains("meme")||beneficiaire.equals(" ")||beneficiaire.equals(""))
lotList.add(new Lot(cc+""+ID_INVOICE, da,NUMERO_QUITANCE,NUMERO_AFFILIE,nomO+" "+prenom,""+(int)(SOLDE),""+PAYED,""+(int)vtva,""+(int)(SOLDE+PAYED),"",""));

     //lotList.add(new Lot(""+ID_INVOICE, da,NUMERO_QUITANCE,NUMERO_AFFILIE,nomO+" "+prenom,""+(int)(SOLDE+PAYED),""+PAYED,""+(int)vtva,""+SOLDE,"",""));
else
lotList.add(new Lot(cc+""+ID_INVOICE, da,NUMERO_QUITANCE,NUMERO_AFFILIE,beneficiaire,""+(int)(SOLDE),""+PAYED,""+(int)vtva,""+(int)(SOLDE+PAYED),"",""));

 soldeClient=soldeClient+(int)(SOLDE); 
tvaYose=tvaYose+vtva;

}
}
else if(nAff.equals("NORMAL"))
{
    String in  = (String)JOptionPane.showInputDialog(null, "Fait votre choix", "Pourcentage de Remise  % ", JOptionPane.QUESTION_MESSAGE, null, new String [] {"0","5","10","15","20","25","30"}, "");
     Integer in_int=new Integer(in);
     double percentage =(double)( 100-in_int.intValue())/100;
     System.out.println("percentage:"+percentage);

     
        d.outResult = d.s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,"
                + "percentage,document, external_data from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "
                        + "and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie "
                                + "and cause='M' order by INVOICE.ID_INVOICE ");
    System.out.println("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document, external_data "
            + "from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "
                    + "and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie "
                            + "and cause='M' order by INVOICE.ID_INVOICE ");

        while (d.outResult.next())
    {

                int  ID_INVOICE=  d.outResult.getInt(1);
                String NUMERO_AFFILIE=d.outResult.getString(2);
                String NUMERO_QUITANCE=  d.outResult.getString(3);
                int tva =(int)(d.outResult.getInt(9)*percentage);
                int SOLDE=(int)((d.outResult.getInt(4))*percentage);
//                int PAYED =(int)((d.outResult.getInt(5))*percentage);
                int PAYED = 0;
                if(d.outResult.getInt(10) != 0 && d.outResult.getInt(5) == 0)
                {
                    PAYED = getCashPayed(ID_INVOICE, d.stat);
                }else{
                    PAYED = (int)((d.outResult.getInt(5))*percentage);
                }
                String da =d.outResult.getString(6);
                String nomO =d.outResult.getString(7);
                String prenom =d.outResult.getString(8); 
                int vtva=(int)((100-d.outResult.getInt(10))*tva)/100;
                String externalData = outResult.getString(12);
                String numSDC = "";
                if(!externalData.isEmpty() && externalData.contains("SDC")){
                    String [] data = externalData.split(",");
                    numSDC = data[0]+","+data[1]+"/"+data[2];
                }

 credit.add(new  Credit( ID_INVOICE, NUMERO_AFFILIE , NUMERO_QUITANCE, SOLDE, PAYED,da,nomO,prenom,vtva,SOLDE+PAYED)); 
 
lotList.add(new Lot(numSDC, da,NUMERO_QUITANCE,NUMERO_AFFILIE,nomO+" "+prenom,""+(int)(SOLDE),""+PAYED,
        ""+(int)vtva,""+(int)(SOLDE+PAYED),cc+""+ID_INVOICE,""));
soldeClient=soldeClient+(int)(SOLDE); 
tvaYose=tvaYose+vtva;
     }

}
else{
    //System.out.println("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,numero_quitance,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and  APP.INVOICE.DATE LIKE '%"+d+"%'and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' order by INVOICE.ID_INVOICE ");

d.outResult = d.s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,numero_quitance,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' order by INVOICE.ID_INVOICE ");
     while (d.outResult.next())
    {
               int  ID_INVOICE=d.outResult.getInt(1);
                String NUMERO_AFFILIE=d.outResult.getString(2);
                String NUMERO_QUITANCE= d.outResult.getString(3);
                int SOLDE=d.outResult.getInt(4);
//              int PAYED = d.outResult.getInt(5);
                int PAYED = 0;
                if(d.outResult.getInt(10) != 0 && d.outResult.getInt(5) == 0)
                {
                    PAYED = getCashPayed(ID_INVOICE, d.stat);
                }else{
                    PAYED = d.outResult.getInt(5);
                }
                String da =d.outResult.getString(6);
                String nomO =d.outResult.getString(7);
                String prenom =d.outResult.getString(8);
                int tva =d.outResult.getInt(9);
                int vtva=(int)((100-d.outResult.getInt(10))*tva)/100;
                // System.out.println(nomO);
                credit.add(new  Credit( ID_INVOICE, NUMERO_AFFILIE , NUMERO_QUITANCE, SOLDE, PAYED,da,nomO,prenom,vtva)); 

   }
String J  = (String)JOptionPane.showInputDialog(null, "Fait votre choix", "PRINT",
JOptionPane.QUESTION_MESSAGE, null, new String [] {"S TOTAL","GENERAL"}, "");


makePrint(J);
credit=new LinkedList();
} 

     d.outResult.close();
      }  catch (Throwable e)  {
            db.insertError(e+""," V getcredit");
    System.out.println(" . .credit .. exception thrown:"+e);
     }
}


public void  getCredit(String stock)
{ 
    try
    {

String dbb="";
String nAff  = (String)JOptionPane.showInputDialog(null, "Fait votre choix", "PRINT EXCEL",
JOptionPane.QUESTION_MESSAGE, null, new String [] {"NORMAL","SPECIAL"}, "");
 

 String  Debut = JOptionPane.showInputDialog("ENTREZ LE TEMPS du debut",db.getString("debut"));
 String FIN = JOptionPane.showInputDialog("ENTREZ LE TEMPS de la fin ",db.getString("fin"));

if(stock.equals("RAMA"))
dbb="_RAMA";
if(stock.equals("RAMA"))
{ dbb="_RAMA";

outResult = s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,"
        + "document,beneficiaire from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE   invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' "
                + "and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and "
                        + "cause='M' order by INVOICE.ID_INVOICE ");
System.out.println("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document,beneficiaire from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE   invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' order by INVOICE.ID_INVOICE ");

while (outResult.next())
{ 
                int  ID_INVOICE= outResult.getInt(1);
                String NUMERO_AFFILIE= outResult.getString(2);
                String NUMERO_QUITANCE=   outResult.getString(3);
                int SOLDE= outResult.getInt(4);
//                int PAYED =outResult.getInt(5);
                int PAYED = ((SOLDE / (100 - outResult.getInt(10))) * outResult.getInt(10));//d.outResult.getInt(5);
                String da =outResult.getString(6);
                String nomO =outResult.getString(7);
                String prenom =outResult.getString(8); 
                int tva =outResult.getInt(9);
                int vtva=(int)((100- outResult.getInt(10))*tva)/100; 
                String  beneficiaire= outResult.getString(12);  
 System.out.println(":"+beneficiaire+"**");
 credit.add(new  Credit( ID_INVOICE, NUMERO_AFFILIE , NUMERO_QUITANCE, SOLDE, PAYED,da,nomO+" "+prenom,beneficiaire,vtva) );
 
 if(beneficiaire.contains("MEME")||beneficiaire.contains("meme")||beneficiaire.equals(" ")||beneficiaire.equals(""))
lotList.add(new Lot( ""+ID_INVOICE, da,NUMERO_QUITANCE,NUMERO_AFFILIE,nomO+" "+prenom,""+(int)(SOLDE),""+PAYED,""+(int)vtva,""+(int)(SOLDE+PAYED),"",""));

     //lotList.add(new Lot(""+ID_INVOICE, da,NUMERO_QUITANCE,NUMERO_AFFILIE,nomO+" "+prenom,""+(int)(SOLDE+PAYED),""+PAYED,""+(int)vtva,""+SOLDE,"",""));
else
lotList.add(new Lot( ""+ID_INVOICE, da,NUMERO_QUITANCE,NUMERO_AFFILIE,beneficiaire,""+(int)(SOLDE),""+PAYED,""+(int)vtva,""+(int)(SOLDE+PAYED),"",""));

 soldeClient=soldeClient+(int)(SOLDE); 
tvaYose=tvaYose+vtva;

}
}
else if(nAff.equals("NORMAL"))
{
    String in  = (String)JOptionPane.showInputDialog(null, "Fait votre choix", "Pourcentage de Remise  % ", JOptionPane.QUESTION_MESSAGE, null, new String [] {"0","5","10","15","20","25","30"}, "");
     Integer in_int=new Integer(in);
     double percentage =(double)( 100-in_int.intValue())/100;
     System.out.println("percentage:"+percentage);

     
        outResult = s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,"
                + "document from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and  "
                        + "APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie "
                                + "and cause='M' order by INVOICE.ID_INVOICE ");
    System.out.println("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,NUMERO_QUITANCE,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE  invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' order by INVOICE.ID_INVOICE ");

        while (outResult.next())
    {

                int  ID_INVOICE=  outResult.getInt(1);
                String NUMERO_AFFILIE=outResult.getString(2);
                String NUMERO_QUITANCE=  outResult.getString(3);
                int tva =(int)(outResult.getInt(9)*percentage);
                int SOLDE=(int)((outResult.getInt(4))*percentage);
//                int PAYED =(int)((outResult.getInt(5))*percentage);
                int PAYED = (int)((SOLDE / (100 - percentage)) * percentage);//d.outResult.getInt(5);
                String da =outResult.getString(6);
                String nomO =outResult.getString(7);
                String prenom =outResult.getString(8); 
                int vtva=(int)((100-outResult.getInt(10))*tva)/100;

 credit.add(new  Credit( ID_INVOICE, NUMERO_AFFILIE , NUMERO_QUITANCE, SOLDE, PAYED,da,nomO,prenom,vtva,SOLDE+PAYED)); 
 
lotList.add(new Lot( ""+ID_INVOICE, da,NUMERO_QUITANCE,NUMERO_AFFILIE,nomO+" "+prenom,""+(int)(SOLDE),""+PAYED,""+(int)vtva,""+(int)(SOLDE+PAYED),"",""));
soldeClient=soldeClient+(int)(SOLDE); 
tvaYose=tvaYose+vtva;
     }

}
else{
    //System.out.println("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,numero_quitance,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and  APP.INVOICE.DATE LIKE '%"+d+"%'and  APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and cause='M' order by INVOICE.ID_INVOICE ");

outResult = s.executeQuery("select APP.CREDIT.ID_INVOICE,NUMERO_AFFILIE,numero_quitance,SOLDE, PAYED,date,NOM_CLIENT,PRENOM_CLIENT,tva,percentage,document "
        + "from  APP.CREDIT,APP.INVOICE,APP.CLIENT"+dbb+" WHERE invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' and "
                + " APP.INVOICE.NUM_CLIENT='"+stock+"' and  CREDIT.ID_INVOICE =INVOICE.ID_INVOICE and num_affiliation=numero_affilie and "
                        + "cause='M' order by INVOICE.ID_INVOICE ");
     while (outResult.next())
    {
               int  ID_INVOICE=outResult.getInt(1);
                String NUMERO_AFFILIE=outResult.getString(2);
                String NUMERO_QUITANCE= outResult.getString(3);
                int SOLDE=outResult.getInt(4);
//                int PAYED =outResult.getInt(5)
                int PAYED = ((SOLDE / (100 - outResult.getInt(10))) * outResult.getInt(10));//d.outResult.getInt(5);
                String da =outResult.getString(6);
                String nomO =outResult.getString(7);
                String prenom =outResult.getString(8);
                int tva =outResult.getInt(9);
                int vtva=(int)((100-outResult.getInt(10))*tva)/100;
                // System.out.println(nomO);
                credit.add(new  Credit( ID_INVOICE, NUMERO_AFFILIE , NUMERO_QUITANCE, SOLDE, PAYED,da,nomO,prenom,vtva)); 

   }
String J  = (String)JOptionPane.showInputDialog(null, "Fait votre choix", "PRINT",
JOptionPane.QUESTION_MESSAGE, null, new String [] {"S TOTAL","GENERAL"}, "");


makePrint(J);
credit=new LinkedList();
} 

     outResult.close();
      }  catch (Throwable e)  {
            db.insertError(e+""," V getcredit");
    System.out.println(" . .credit .. exception thrown:"+e);
     }
}


void makePrint ( String option)
{
  
lotList =new LinkedList ();

int totalG=0;
int tvaG=0;

for (int i =0;i<credit.size();i++)
{
double total=0;
double tva  =0;
int k=0;
Credit cre=credit.get(i);
double percentage=((double)cre.SOLDE/(double)(cre.SOLDE+cre.PAYED));
try {
outResult = s.executeQuery("select PRODUCT.NAME_PRODUCT,LIST.PRICE,LIST.QUANTITE,product.tva from  list,product WHERE list.id_invoice="+cre.ID_INVOICE+" and  LIST.CODE_UNI=PRODUCT.CODE");
while (outResult.next())
{
int price=(int) (outResult.getDouble(2));
int subTot=(int)((price*outResult.getInt(3)*(percentage ))+0.3);
int tot =((price*outResult.getInt(3)));
double t= (outResult.getDouble(4));
int h =(int)((price)/(1+t));
 
int ttva=(int)(price*t);
//System.out.println(price+"   "+subTot+"   "+percentage);
  if(k==0  )
    {
if(option.equals("S TOTAL"))
{
lotList.add(new Lot(""+cre.ID_INVOICE, cre.NUM_CLIENT,cre.NUMERO_QUITANCE,cre.NUMERO_AFFILIE,cre.NOM+" "+cre.PRENOM,outResult.getString(1),""+outResult.getInt(3),""+(int)price,""+(int)subTot,"",""));
}
if(option.equals("GENERAL"))
{
lotList.add(new Lot(""+cre.ID_INVOICE, cre.NUM_CLIENT,cre.NUMERO_QUITANCE,cre.NUMERO_AFFILIE,cre.NOM+" "+cre.PRENOM,outResult.getString(1),""+outResult.getInt(3),""+(int)price,""+(int)tot,""+(int)subTot,""));
}
}
else
{
if(option.equals("S TOTAL"))
{
lotList.add(new Lot("","","","","",outResult.getString(1),""+outResult.getInt(3),""+(int)price,""+(int)subTot,"",""));
}
if(option.equals("GENERAL"))
{
lotList.add(new Lot(""+cre.ID_INVOICE, cre.NUM_CLIENT,cre.NUMERO_QUITANCE,cre.NUMERO_AFFILIE,cre.NOM+" "+cre.PRENOM,outResult.getString(1),""+outResult.getInt(3),""+(int)price,""+(int)tot,""+(int)subTot,""));
} 
}
total=total+subTot ;
tva=tva+ttva;
k++;
}
if(option.equals("S TOTAL"))
{ 
lotList.add(new Lot("","","","","","     -------------------","0","0",""+(int)total,"",""));
} 
} catch (Throwable ex) { ;
 }
 totalG=totalG+(int)total;
 tvaG=tvaG+(int)tva;
 soldeClient=soldeClient+cre.SOLDE;
 CashClient=CashClient+cre.PAYED;
 tvaYose=tvaYose+(int)tva;
}
mergeName(lotList);
}
void mergeName(LinkedList <Lot> huza )
{
   // for(int i=0;i<huza.size();i++)
   // System.out.println(huza.get(i).u6+" qte "+huza.get(i).u7);

LinkedList <Lot> join=new LinkedList ();
int j=0;
int qte=0;
int price=0;
int subT=0;
boolean done=true;


//System.out.println( " ==================================================================== ");
for(int i=0;i<huza.size()-1;i++)
{
Lot now=huza.get(i);
j=i+1;
qte=Integer.parseInt(now.u7);
price=Integer.parseInt(now.u8);
subT=Integer.parseInt(now.u9);

//System.out.println(j+huza.get(j).u6+" ====================== "+i+now.u6);
if(now.u6.equals(huza.get(j).u6) && huza.get(j).u1.equals(""))
{
while(now.u6.equals(huza.get(j).u6) && huza.get(j).u1.equals("") )
{
qte=qte+ Integer.parseInt(huza.get(j).u7);
//System.out.println(huza.get(j).u6+" ===========IN=========== "+subT);
subT=subT+Integer.parseInt(huza.get(j).u9);

//"","","","","",outResult.getString(1),""+outResult.getInt(3),""+price,""+subTot,"",""
i=j;
j++;

done=false;
}}

if(qte==0)
 join.add(now);
else
join.add(new Lot(now.u1,now.u2,now.u3,now.u4,now.u5,now.u6,""+qte,""+price,""+(subT),now.u0,now.u11));

if(done)
{
//System.out.println(now.u6+" qte "+qte);
//i=j;
}
}

//System.out.println( " ==================================================================== ");

//for(int i=0;i<join.size();i++)
  //  System.out.println(join.get(i).u6+" after qte "+join.get(i).u7);
lotList=join;
}
    
 @Override
protected void processWindowEvent(WindowEvent evt)
{
if(WindowEvent.WINDOW_CLOSING == evt.getID())
{

System.exit(0);
}
//
}
  public static int getIntVal(String s, String s1, int i) {
        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " ENTRER UN NOMBRE ", "EXEMPLE : 1");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int.intValue();
                    done = false;
                    if (entier < 0) {
                        entier = -1;
                    }
                }

            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + " N'EST PAS UN NOMBRE; VEUILLEZ ENTRER UN NOMBRE SVP !");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(null, "NOMBRE D'ESSAIE TERMINÉS ", " DÉSOLEZ NOUS DEVONS VOUS QUITTER ! ", JOptionPane.PLAIN_MESSAGE);

        }

        return entier;
    }
  
  int  getCashPayed(int ID_INVOICE, Statement stat)
  {
      try{
          String sql = "select * from app.INVOICE where REFERENCE = 'CASHNORM_" + ID_INVOICE + "'";
          System.out.println(">>" + sql);
          ResultSet res = stat.executeQuery(sql);
          while(res.next())
          {
              return (int)res.getDouble("CASH");
          }
      }catch(SQLException e)
      {
          ;
      }
      return 0;
  }
public static void main(String[] arghs) throws IOException
{
   
        BufferedReader entree = null;
        try {
            LinkedList<String> give = new LinkedList();
            File productFile = new File("log.txt");
            String ligne = null;
            entree = new BufferedReader(new FileReader(productFile));
            try {
                ligne = entree.readLine();
            } catch (IOException e) {
                System.out.println(e);
            }
            int i = 0;
            while (ligne != null) {
                // System.out.println(i+ligne);
                give.add(ligne);
                i++;

                try {
                    ligne = entree.readLine();
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, "Ikibazo", " reba log ", JOptionPane.PLAIN_MESSAGE);
                }
            }
            entree.close();
            String dataBase = give.get(2);
            String serv = give.get(1);
            String format=give.get(3);
            String lesDataBase = give.get(15);
            new Main(serv,dataBase,format, lesDataBase);
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                entree.close();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}
}