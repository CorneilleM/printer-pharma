 
package printa;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.StringTokenizer; 
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Kimenyi
 */
public class PrintSetup extends JFrame {

LinkedList <Lot> lotList;
int pageNum = 0;
private Properties properties = new Properties();
int margin=0;
int numero;
double total,tva;
Frss frss;
String date,reference,mode;
String [] footer;
AutoOut db;
 Graphics2D g2 ;
 int xPress=0;
 int yPress=0;
 int xRelease=0;
 int yRelease=0;
 Variable vari [] =null;
 int uburebure=20;
 int ubugari=20;
  Image img=null;
PrintSetup(LinkedList <Lot> lotList,AutoOut db,String mode ,String date,Frss frss,int numero,int tva,double total,String [] footer)
{
   
this.lotList=lotList;
this.db=db;
this.mode=mode; 
 this.date=date;
this.frss=frss;
this.numero=numero;
this.tva=tva;
this.total=total;
this.footer=footer; 
 
     BufferedInputStream bis = null;
     try {
    bis = new BufferedInputStream(new FileInputStream(getString("logoV")));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    
    int ch;
    while ((ch = bis.read()) != -1) {
    baos.write(ch);
    System.err.println(ch);
    }
    System.err.println(img);
    img = Toolkit.getDefaultToolkit().createImage(baos.toByteArray());
     System.err.println(img);
    System.err.println(img.getWidth(rootPane));
    } catch (IOException exception) {
    System.err.println("Error loading: ");
    }
uburebure = Integer.parseInt(JOptionPane.showInputDialog(null, " Selection Y ","40"));
ubugari= Integer.parseInt(JOptionPane.showInputDialog(null, " Selection X ","40"));
String facture= getString("factureV") + (numero);
enableEvents(WindowEvent.WINDOW_CLOSING);
//on fixe  les composant del'interface
getParametres("PRINT",mode);
ecouteSouris(); 
setSize(602,782);
setVisible(true); 

} 
    @Override
public void paint(Graphics pg)
{ 
    //782 getPageDimension 602
pg.clearRect(0, 0, 602, 782);
    int pageH = 782;
    int pageW =602;
  
  if (pg != null)
  {
    this.margin=getValue("margin"); 
 
    try {
    Font helv = new Font("Helvetica", Font.BOLD, getValue("dateT"));
    pg.setFont(helv);
  
    pg.drawString(getString("ville")+", le " + date.substring(0, 2) + "/" + date.substring(2, 4) + "/" + date.substring(4, 6),  getValue("dateX"), getValue("dateY"));
    helv = new Font("Helvetica", Font.BOLD, 25);
    pg.setFont(helv);
    
 
    pg.drawImage(img, getValue("logoX"), getValue("logoY"), getValue("marginPrix"), getValue("longeurPrix"), rootPane);

 
    helv = new Font("Helvetica", Font.PLAIN, getValue("adresseT"));
    //have to set the font to get any output
    pg.setFont(helv);
    FontMetrics fm = pg.getFontMetrics(helv);
    int fontHeight = fm.getHeight();
    int fontDescent = fm.getDescent();

    pg.drawString(getString("adresseV"), getValue("adresseX"),  getValue("adresseY"));

    pg.drawString(getString("rueV"), getValue("rueX"),  getValue("rueY"));

    pg.drawString(getString("localiteV"), getValue("localiteX"),  getValue("localiteY"));

    pg.drawString(getString("telV"), getValue("telX"), getValue("telY"));

    pg.drawString(getString("faxV"), getValue("faxX"), getValue("faxY"));
    pg.drawString(getString("emailV"), getValue("emailX"), getValue("emailY"));
    pg.drawString(getString("tvaV"), getValue("tvaX"),  getValue("tvaY"));
    pg.drawRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), getValue("clientLargeur"));
    Font helv2 = new Font("Helvetica", Font.BOLD, getValue("clientT"));
    pg.setFont(helv2);

    pg.drawString(frss.NOM_CLIENT, getValue("clientX") + 10,  getValue("clientY") + 10);
    helv2 = new Font("Helvetica", Font.ITALIC, getValue("clientT")-2);
    pg.setFont(helv2);
    pg.drawString(frss.PRENOM_CLIENT, getValue("clientX" ) + 10, getValue("clientY" ) + 20);
    pg.drawString(frss.ln2, getValue("clientX" ) + 10,  getValue("clientY" ) + 30);
    pg.drawString(frss.ln3, getValue("clientX" ) + 10, getValue("clientY" ) + 40);
    pg.drawString(frss.SECTEUR, getValue("clientX" ) + 10,  getValue("clientY" ) + 50);
    Font helv1 = new Font("Helvetica", Font.BOLD, getValue("factureT"));
    
    String facture="";
  System.err.println(" 2 Error loading: ");
             facture=getString("factureV") + (numero);
    pg.setFont(helv1);
    pg.drawString(facture, getValue("factureX"), getValue("factureY"));

    helv2 = new Font("Helvetica", Font.PLAIN, getValue("referenceT"));
    pg.setFont(helv2);
   // pg.drawString(getString("referenceV") + reference, getValue("referenceX"), getValue("referenceY"));
    helv1 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv1);
    //pageNum++;
   // pg.drawString("Page  " + pageNum, pageW / 2, pageH - 9);

    helv2 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv2);
            int g = getValue("length");
            String[] entete = new String [g];
            int[] longeur = new int[g+1];
            String[] variable= new String [g];
            int [] dimension=new int[g];
            longeur[0]=0;
 System.err.println("Error 333333333333333333333333333 loading: "); 
            int page=1;
            int y=  getValue("entete"+page+"X");
            for(int k=0;k<g;k++)
            {
            entete[k]=getString("entete_"+(k+1)+"Valeur");
            longeur[k+1]=getValue("entete_"+(k+1)+"Largeur");
          
            variable[k]=getString("entete_"+(k+1)+"Variable");
            dimension[k]=getValue("entete_"+(k+1)+"Dimension");
 // System.out.println(" d   "   +getValue("entete_"+(k+1)+"Dimension"));
            }

            int w = margin;

            int curHeightLigne = getValue("ligne"+page+"X");

            Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
            pg.setFont(helvLot);
            FontMetrics fmLot = pg.getFontMetrics(helvLot);
            fontHeight = fmLot.getHeight();
            boolean done = true;

for (int j = 0; j < lotList.size(); j++)
{

            helvLot = new Font("Helvetica", Font.PLAIN, getValue("tailleLigne"));
            pg.setFont(helvLot);
            Lot ci = lotList.get(j);
            System.out.println(ci.u1);
            w = margin;

            if (done) {
            curHeightLigne += fontHeight;
            } else {
            curHeightLigne += 2 * fontHeight;
            }
            done = true;
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            String valeur=ci.vars[i];

            String variab=variable[i];
            int dim      =dimension[i];
           //System.out.println(valeur+" dim  "+dim); setVirguleD(String frw)
            if(variab.equals("int"))
            printInt(pg,setVirgule(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);

            else  if(variab.equals("double"))
            printInt(pg,setVirguleD(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);
         else  if(variab.equals("centreInt"))
            printIntCentre(pg,setVirguleD(valeur), w + (longeur[i + 1]) , curHeightLigne,longeur[i + 1]);
            else  if(variab.equals("centreS"))
            printStringCentre(pg,valeur , w   , curHeightLigne,longeur[i + 1]);
                else if (valeur.length() < dim)
            {
            pg.drawString(valeur, w + 5, curHeightLigne);
            }
            else
            {
            done = false;
            pg.drawString(valeur.substring(0, dim - 1), w + 5, curHeightLigne);
            pg.drawString(valeur.substring(dim - 1, valeur.length()), w + 5, fontHeight + curHeightLigne);
            }
            }

            System.out.println(j+" jjjjj  "+curHeightLigne+ "   "+( getValue("longeurLigne"))); 

            int hauteur = pageH - getValue("marginfooter");

            if ((curHeightLigne+20) >  hauteur-20) {

            /////////////////////////////////////////////////////////////////////////////////////////////
          
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
             if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
   
             facture=getString("factureV") + (numero);
            pg.drawString(facture, margin, margin + 25);
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=1;
        //    pg.drawString("Page  " + pageNum, pageW / 2, pageH - 10);
            curHeightLigne=getValue("entete"+page+"X")+25;
            y=getValue("entete"+page+"X");
            done = true;
            }
 } 
///////////////////////////////////////////////////////////////////////////////////////////////////////////

            if((curHeightLigne+20) > ( pageH -getValue("longeurLigne")) )
            {
                 int hauteur = pageH - getValue("marginfooter");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
           
            if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
         
             facture=getString("factureV") + (numero);
            pg.drawString(facture, margin, margin + 25);

            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2, pageH - 10);
              curHeightLigne=getValue("entete"+page+"X")+25;
            done = true;
            hauteur = pageH - getValue("longeurLigne");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete

            }
            else
            {

            int hauteur = pageH - getValue("longeurLigne");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete
            }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        w = margin;
                        helv2 = new Font("Helvetica", Font.BOLD, getValue("prixTaille"));
                        pg.setFont(helv2);
                        int lp = getValue("lengthPrix");
                        String[] enteteP = new String [lp+1];
                        int[] longeurP = new int[lp+1];
                        String[] variableP= new String [lp];
                        longeurP[0]=0;



                        for(int k=0;k<lp;k++)
                        {
                        
                        enteteP[k]=getString("prix_"+(k+1)+"Valeur");
                        if(enteteP[k].length()>2)
                        enteteP[k]=enteteP[k]+frss.devise;
                        longeurP[k+1]=getValue("prix_"+(k+1)+"Largeur");
                        variableP[k]=getString("prix_"+(k+1)+"Variable");
                        System.out.println(longeurP[k+1]);
                        }
                        int Xprix = getValue("longeurY");
                        int marginP=getValue("longeurX");
                        System.out.println(getValue("longeurX")+"  ---  "+getValue("longeurY"));
                        w =marginP ;
                        for (int i = 0; i < lp; i++) {
                        w += (longeurP[i]);
                        
                        System.out.println(( w + (longeurP[i + 1]) - 40)+"  kkk "+(Xprix+25)+"  total "+total+"   tva "+tva);

                        int wid=w + (longeurP[i + 1]) - 20;
                        if(i==0)
                        printInt(pg,setVirgule( ""+(int)(total-tva)),  wid, Xprix+15);
                        if(i==1)
                        printInt(pg,setVirgule(""+ (int)tva),  wid, Xprix+15);
                        if(i==2)
                        printInt(pg,setVirgule( ""+(int)(total)),  wid, Xprix+15);
 
                    pg.drawLine(xRelease,yRelease-50, xRelease,yRelease+50);       
 pg.drawLine(xRelease-50,yRelease, xRelease+50,yRelease);
                        pg.drawLine(w,Xprix-20, w, Xprix+20);//ligne zihagaze
                      //  System.out.println( w +" x1 y1 "+(Xprix-20)+"  y2 "+ Xprix+20);
                        pg.drawString(enteteP[i], w + 2, Xprix - 5);// amazina
                       // System.out.println(enteteP[i]+"entete   x=w+2   yxprix-5= "+(Xprix - 5));
//+frss.devise
                        }
                         if(lp!=0)
                        {
                        pg.drawLine(marginP, Xprix, w, Xprix);//umurongo wo hasi
                       // System.out.println(marginP+"marginP  lineMidlle y=xprix   y= "+w);
                        pg.drawRect(marginP, Xprix-20 , w-marginP , 40); // cadre ya entete
                     //    System.out.println(marginP+" x   cadre y Xprix-20"+( Xprix-20)+" length= "+(w-marginP));

                        } 
                          
                       helv2 = new Font("Helvetica", Font.PLAIN, 8); 
                       
                       //if(mode.equals("COMMANDE"))
                        pg.setFont(helv2);
                        
                                    if(mode.equals("CALCUL"))
                                     for(int v=1;v<getValue("lengthfooter") && footer.length>v;v++)
                                     {
                                         System.out.println( getValue("footer_"+v+"X" )+" dedede "+footer [v]+getValue("footer_"+v+"Y" ));
                                         pg.drawString( footer [v] , getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));
                                     }

                                    else
                                   for(int v=1;v<getValue("lengthfooter") ;v++)
                                        if(v<6)
                                    pg.drawString(getString("footer_"+v+"Valeur")+  ""+footer[v], getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));
                                        else
                                    pg.drawString(getString("footer_"+v+"Valeur"), getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));


    } catch (Throwable ex) {
                System.out.println(ex);
            }  
  }

}
    public void printIntCentre(Graphics pg,String s,int w,int h,int length)
  {

int back=0;
int center=w-(length/2)+((6*s.length())/2);
  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=6;
      pg.drawString(""+s.charAt(i),center-back,h);
      System.out.print (s.charAt(i)+" - "+(center-back));
  }
}
public void printStringCentre(Graphics pg,String s,int w,int h,int length)
  {
int back=0;
  int f=(length-s.length())/2;
  int center=w+f-16;
  
  System.out.println(w+"   "+s+"   "+f+"   "+length);
     
for(int i=0;i<s.length();i++)
  {
      pg.drawString(""+s.charAt(i),center+back,h);
      System.out.print (s.charAt(i)+" - "+(center+back));
      if(s.charAt(i)=='I')
       back+=3;
      else  if(s.charAt(i)=='W' || s.charAt(i)=='M')
       back+=8;
      else
       back+=6;

    
  }

}

public void ecouteSouris()
{
this.addMouseListener(new MouseListener() {
{}
public void actionPerformed(ActionEvent ae)
{}
public void mouseExited(MouseEvent ae)
{}
public void mouseClicked(MouseEvent e)
{
     
}
public void mousePressed(MouseEvent e)
{
  xPress=e.getX();
  yPress=e.getY();
  

};
public void mouseReleased(MouseEvent e)
{
 //JOptionPane.showMessageDialog(null,xPress+"  x    y "+yPress," press ",JOptionPane.PLAIN_MESSAGE); 
 
 LinkedList<Variable> paraGood= new LinkedList () ;
 for( int i=0;i<vari.length;i++)
 {
     if(vari[i]!=null)
     {
          //System.out.println(vari[i]+"  x  ---  y "+i);
         System.out.println(xPress+"  x  ---  x "+vari[i].value+"  x  ---  x "+vari[i].nom+"    ---   "+vari[i].value2+"  y  ---  y "+yPress);
     
     int variaX=(int)Double.parseDouble(vari[i].value);
     int variaY=(int)Double.parseDouble(vari[i].value2); 
     System.out.println((variaX< xPress+ubugari && variaX > xPress-ubugari)+"  x  ---  x "+(variaY<=yPress+uburebure && variaY>=yPress-uburebure) );
     if((variaX< xPress+ubugari && variaX > xPress-ubugari) && (variaY<=yPress+uburebure && variaY>=yPress-uburebure))
     {
         paraGood.add(vari[i]);
     }
     } 
 }
 
 Variable varigood [] = new Variable [paraGood.size()+1]; 
 for(int y=0;y<paraGood.size();y++)
     varigood[y]=paraGood.get(y);//.nom+"  "+paraGood.get(y).value+"  "+paraGood.get(y).value2; 
 varigood[varigood.length-1]=new Variable("ALL","ALL","ALL","ALL"); 
 Variable v = (Variable)JOptionPane.showInputDialog(null, "Faites votre choix", " PRINT",
        JOptionPane.QUESTION_MESSAGE, null, varigood, "");
 

 
xRelease=e.getX();
yRelease=e.getY();

//JOptionPane.showMessageDialog(null,xRelease+"  x    y "+yRelease," released ",JOptionPane.PLAIN_MESSAGE);

if(v!=null)
try 
{   
if(v.nom.equals("ALL"))  
{
   String ch = (String)JOptionPane.showInputDialog(null, "Faites votre choix", " BOUGE",JOptionPane.QUESTION_MESSAGE, null, new String []{"X","Y"}, "");
    
    if(ch.equals("X"))
    {
       int espace = Integer.parseInt(JOptionPane.showInputDialog(null, " espace en Y ","20"));
       int espaceY=yRelease;
    for(int y=0;y<paraGood.size();y++)
    { 
 Variable vi =paraGood.get(y); 
System.out.println(" update APP.VARIABLES set value_variable ='" + xRelease + "'  where nom_variable='" + vi.nom+"Y"+mode + "' ");

db.s.execute(  " update APP.VARIABLES set value_variable ='" + xRelease + "'  where nom_variable='" + vi.nom+"X"+mode + "' ");
db.s.execute(  " update APP.VARIABLES set value_variable ='" + espaceY+ "'  where nom_variable='" + vi.nom+"Y"+mode + "' ");
espaceY=espaceY+espace;

    }
    }
    else
    {
   int espace = Integer.parseInt(JOptionPane.showInputDialog(null, " espace en X ","20"));
    int espaceX=xRelease;
    for(int y=0;y<paraGood.size();y++)
    { Variable vi =paraGood.get(y);  
System.out.println(" update APP.VARIABLES set value_variable ='" + espaceX + "'  where nom_variable='" + vi.nom+"Y"+mode + "' ");

db.s.execute(  " update APP.VARIABLES set value_variable ='" + espaceX + "'  where nom_variable='" + vi.nom+"X"+mode + "' ");
db.s.execute(  " update APP.VARIABLES set value_variable ='" + yRelease+ "'  where nom_variable='" + vi.nom+"Y"+mode + "' ");

espaceX=espaceX+espace;

    }
   }
  
}   
else
{   
System.out.println(" update APP.VARIABLES set value_variable ='" + xRelease + "'  where nom_variable='" + v.nom+"Y"+mode + "' ");

db.s.execute(  " update APP.VARIABLES set value_variable ='" + xRelease + "'  where nom_variable='" + v.nom+"X"+mode + "' ");
db.s.execute(  " update APP.VARIABLES set value_variable ='" + yRelease+ "'  where nom_variable='" + v.nom+"Y"+mode + "' ");
}
getParametres("PRINT",mode);

repaint();

}
catch (SQLException ex) {
db.insertError(ex+"","updateparam"+v.nom);
}

};
public void mouseEntered(MouseEvent e)
{

//JOptionPane.showMessageDialog(null,e.getX()+"  x    y "+e.getY()," enterd ",JOptionPane.PLAIN_MESSAGE);

} 
});

}   
int getValue(String s)
{

int ret=0;
//System.out.println("valu "+s );
Variable var= getParam("PRINT",mode,s+mode);

//System.out.println("valu "+var );


try
{
 if(var!=null)
 {
Double val=new Double (var.value);
ret = val.intValue();
}
}catch (Throwable e)
{
ret=0;
}

return ret;
}
void getParametres(String famille,String mode)
{
LinkedList<Variable> par= new LinkedList () ;
try
{
db.outResult = db.s.executeQuery("select  * from APP.VARIABLES where APP.variables.value2_variable= '"+mode+"' and APP.variables.famille_variable= '"+famille+"' ");

while (db.outResult.next())
{
    System.out.println(db.outResult.getString(2));
        String num =db.outResult.getString(1);
        String value =db.outResult.getString(2);
        String fam =db.outResult.getString(3);
        String value2 =db.outResult.getString(4);
        par.add(new Variable(num,value,fam,value2));
}}
catch (Throwable e){
      db.insertError(e+"","getparm autoout");
 } 
LinkedList<Variable> parX= new LinkedList () ;
LinkedList<Variable> parY= new LinkedList () ;

  for(int i=0;i<par.size();i++)
{

Variable v= par.get(i);
if(v.nom.contains("X"))
{
    parX.add(v);
    System.out.println("X  "+v.nom+"      sans X  "+v.nom.replace("X", ""));
}  
else if(v.nom.contains("Y")) 
{
    parY.add(v);
    System.out.println("Y "+v.nom);
}  
}
  if(parY.size()!=parX.size())
   System.out.println(parX.size()+"Y X   ntizingana "+parY.size());
  
 vari = new Variable [Math.max(parY.size(), parX.size())];
 
int k=0;
for(int i=0;i<parX.size();i++)
{
   Variable varX= parX.get(i);
 String vaX=    varX.nom.replace("X", "").replace(mode, "");
    
    
for(int j=0;j<parY.size();j++)
{
     Variable varY=  parY.get(j);
     String  vaY=varY.nom.replace("Y", "").replace(mode, "");
      
      if(vaX.equals(vaY))
      {
          vari[k]=(new Variable(vaY,varX.value,varX.famille,varY.value));
          System.out.println(vaY+ "  x  "+varX.value+ "  y  "+varY.value);
          k++;
      }
 
    
}
    
    
}



    
}

Variable getParam(String famille,String mode, String nom)
{
 //System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"' and APP.variables.value2_variable= '"+mode+"' and APP.variables.famille_variable= '"+famille+"' ");
    Variable par = null;
try
{
db.outResult = db.s.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"' and APP.variables.value2_variable= '"+mode+"' and APP.variables.famille_variable= '"+famille+"' ");

while (db.outResult.next())
{
  //  System.out.println(db.outResult.getString(2));
        String num =db.outResult.getString(1);
        String value =db.outResult.getString(2);
        String fam =db.outResult.getString(3);
        String value2 =db.outResult.getString(4);
        par=(new Variable(num,value,fam,value2));
}}
catch (Throwable e){
      db.insertError(e+"","getparm autoout");
 } 
return     par;
}

String getString(String s)
{
String ret=" ";
 Variable var= getParam("PRINT",mode,s+mode);
 if(var!=null)
     ret=var.value;
 
     return ret;
}
public void printInt(Graphics pg,int nbre,int w,int h)
  {
  String s=setVirgule(nbre);

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;

      pg.drawString(""+s.charAt(i),w-back,h);

  }
}
public void printInt(Graphics pg,String s,int w,int h)
  {

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;


      pg.drawString(""+s.charAt(i),w-back,h);

  }




  }
  String setVirgule(int frw)
        {

String setString = ""+frw;
int l =setString.length();



if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;

}

return setString;
}
  String setVirgule(String setString)
        {


int l =setString.length();



if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;

}

return setString;
}
  String setVirguleD(String frw)
        {
      String setString="";
if(frw.contains("."))
{
StringTokenizer st1=new StringTokenizer (frw);

    String entier =st1.nextToken(".");
System.out.println(frw);
    String decimal=st1.nextToken("").replace(".", "");

    if(decimal.length()==1 && decimal.equals("0") )
    decimal=".00";
    else if (decimal.length()==1 && !decimal.equals("0") )
          decimal="."+decimal+"0";
    else
    decimal="."+decimal.substring(0, 2);

 setString = entier+decimal;

int l =setString.length();
if(l<2)
   setString = "    "+frw;
else if(l<3)
   setString = "  "+frw;
else if(l<4)
   setString = "  "+frw;

int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
    setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;

}
}
else
  setString=  setVirgule( frw);
return setString;
} 

}
